<?php

namespace clinic;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
	protected $table = 'medicine';
	protected $fillable = array('name', 'category');

}
