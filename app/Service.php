<?php

namespace clinic;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $table = 'services';
	protected $fillable = array( 'name', 'price', 'description' );
}
