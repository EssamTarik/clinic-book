<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

use Carbon\Carbon;

class AgeController extends Controller
{
	public function index(Request $request){
		$date = Carbon::parse($request->only('date')['date']);
		$age = $date->diff(Carbon::today());


		$agejson = array();

		$agejson['years'] = $age->y;
		$agejson['months'] = $age->m;
		$agejson['days'] = $age->d;

		return json_encode($agejson);
	}

	public function dob(Request $request){
		$past = Carbon::today()->subDays($request->day)->subMonths($request->month)->subYears($request->year);

		$dob = array('year'=>$past->format('Y'), 'month'=>$past->format('m'), 'day'=>$past->format('d'));
		
		return json_encode($dob);
	}
}
