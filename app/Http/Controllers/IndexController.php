<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

use clinic\Patient;
use clinic\Service;
use clinic\Medicine;
use clinic\Question;

use Carbon\Carbon;

class IndexController extends Controller
{


	public function index(){
		$patients = Patient::all();
		$services = Service::all();
		$drugs = Medicine::all();
		$questions = Question::all();
		return view('index')->
			with(compact('patients'))->
			with(compact('services'))->
			with(compact('drugs'))->
			with(compact('questions'))->
			with('today', Carbon::today()->format('Y-m-d'))->
			with('week', Carbon::today()->addWeeks(1)->format('Y-m-d'));
	}



}
