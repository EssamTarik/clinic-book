<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

use Illuminate\Support\Facades\Auth;

use clinic\Input;
class MyAuth extends Controller
{
	public function getLogin()
	{
		return view('login');
	}

	public function postLogin(Request $request)
	{
		$credentials = $request->only(array('email', 'password'));
		$this->validate($request, array('email'=>'required|email', 'password'=>'required'));
		if(Auth::attempt($credentials))
			return redirect()->to('/');
		else
			return redirect()->back()->with('info', 'Invalid Credentials');
	}

	public function getLogout()
	{

		Auth::logout();
		return redirect()->to('login');

	}

}
