<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

class UserController extends Controller
{
	public function index(){
		return view('edit-user');
	}

	public function editUser(Request $request){
		$this->validate($request,array(
			'name' => 'required',
			'email' => 'required|email',
			'password' => 'confirmed'
		));


		try{
			var_dump($request->all());
			$img_dir = base_path(implode(DIRECTORY_SEPARATOR, 
				array('public','assets','layouts','layout5','img')));

			$success = null;

			if($request->hasFile('picture'))
				$success = $request->picture->move($img_dir, 'avatar1.jpg');
			var_dump($success);

			$user = \Auth::user();
			$user->name = $request->name;
			$user->email = $request->email;
			if($request->password)
				$user->password = bcrypt($request->password);
			$user->save();

			return redirect()->action('IndexController@index')->with('info', 'User Account Edited');
		}catch(\Exception $e){
			return redirect()->url('IndexController@index')->with('error', 'User Account Editing Failed');
		}
	}
}
