<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

use clinic\Question;

class QuestionController extends Controller
{

	public function index(){
		$questions = Question::all();
		return view('questions')->with(compact('questions'));
	}

	public function createQuestion(Request $request){
		$this->validate($request, array(
				'text' => 'required',
			));
		$success = Question::create($request->only('text'));

		if($request->ajax()){
			echo $success->id;
			return ;
		}

		if ($success)
			return redirect()->back()->with('info', 'new question created');
		else
			return redirect()->back()->with('error', 'creation failed');
	}

	public function deleteQuestion(Request $request){
		$ids = array_keys($request->except(array('_token', '_method', 'sample_1_length')));
		foreach ($ids as $id){
			try{
				Question::find($id)->delete();
			
			}catch(\Illuminate\Database\QueryException $e){

				return redirect()->back()->with('error','the following question : ' .Question::find($id)->name . " , cannot be deleted as some visits depend on it");

			}
		}
		return redirect()->back()->with('info', 'selected entries were deleted');
	}
}
