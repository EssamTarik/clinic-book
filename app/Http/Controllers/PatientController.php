<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

use clinic\Patient;

use Carbon\Carbon;

class PatientController extends Controller
{

	public function createPatient(Request $request){

		// $this->validate($request, array(
		// 	'name' => 'required|alpha_num',
		// 	'Consangunity' => 'required',
		// 	'dob'=>'required|date_format:yyyy-mm-dd',
		// ));

		if (Carbon::parse($request->dob)->gt(Carbon::today())){
			echo "the date you entered is in the future !!!!";
			return ;
		}

		$data = $request->all();
		$data['dob'] = Carbon::parse($data['dob']);
		Patient::create($data);
		return Response("New Patient Created");

	}

	public function patientInfo(Request $request){
		$id = (int)$request->id;
		$patient = Patient::find($id);

		$patientjson = array();
		$patientjson['name'] = $patient->name;
		$patientjson['dob'] = Carbon::parse($patient->dob)->format('Y-m-d');
		$patientjson['gender'] = $patient->gender;
		$patientjson['consangunity'] = $patient->Consangunity;

		$patientjson = json_encode($patientjson);
		echo $patientjson;

	}

	public function updatePatient(Request $request){
	
		if (Carbon::parse($request->patient_dob)->gt(Carbon::today())){
			echo "the date you entered is in the future !!!!";
			return ;
		}	
		$patient = Patient::find((int)$request->patient_id);
		$patient->name = $request->patient_name;
		$patient->gender = $request->patient_gender;
		$patient->Consangunity = $request->patient_consangunity;
		$patient->dob = Carbon::parse($request->patient_dob);
		$patient->save();

		echo "Patient Updated";
	}

}
