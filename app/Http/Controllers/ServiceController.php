<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

use clinic\Service;
class ServiceController extends Controller
{

	public function index(){

		$services = Service::all();
		return view('services')->with(compact('services'));
	}

	public function createService(Request $request){
		$this->validate($request, array(
				'name' => 'required',
				'price' => 'numeric',
			));
		$success = Service::create($request->only('name', 'price', 'description'));
		if ($request->ajax()){
			echo $success->id ;
			return ;
		}
		if ($success)
			return redirect()->back()->with('info', 'new service created');
		else
			return redirect()->back()->with('error', 'creation failed');
	}

	public function deleteService(Request $request){
		$ids = array_keys($request->except(array('_token', '_method', 'sample_1_length')));
		foreach ($ids as $id){
			try{
				Service::find($id)->delete();
			
			}catch(\Illuminate\Database\QueryException $e){

				return redirect()->back()->with('error','the following service : ' .Service::find($id)->name . " , cannot be deleted as some visits depend on it");

			}
		}
		return redirect()->back()->with('info', 'selected entries were deleted');
	}

}
