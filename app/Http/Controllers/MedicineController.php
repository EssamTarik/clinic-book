<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

use clinic\Medicine;

class MedicineController extends Controller
{

	public function index(){
		$drugs = Medicine::all();
		return view('drugs')->with(compact('drugs'));
	}

	public function createMedicine(Request $request){

		$this->validate($request, array(
				'name' => 'required',
			));
		$success = Medicine::create($request->only('name', 'category'));
		if($request->ajax()){
			echo $success->id;
			return ;
		}
		if ($success)
			return redirect()->back()->with('info', 'new medicine created');
		else
			return redirect()->back()->with('error', 'creation failed');

	}


	public function deleteMedicine(Request $request){

		$ids = array_keys($request->except(array('_token', '_method', 'sample_1_length')));
		foreach ($ids as $id){
			try{
				Medicine::find($id)->delete();
			
			}catch(\Illuminate\Database\QueryException $e){

				return redirect()->back()->with('error','the following drug : ' .Medicine::find($id)->name . " , cannot be deleted as some visits depend on it");

			}
		}
		return redirect()->back()->with('info', 'selected entries were deleted');
		
	}
}
