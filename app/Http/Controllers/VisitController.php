<?php

namespace clinic\Http\Controllers;

use Illuminate\Http\Request;

use clinic\Http\Requests;

use clinic\Patient;

use clinic\Visit;

use clinic\Service;

use clinic\Medicine;

use clinic\Question;

use Carbon\Carbon;

class VisitController extends Controller
{


	public function index(){
		return view('history');
	}

	public function details(Request $request){
		$visit = Visit::find($request->id);
		$service = Service::find($visit->service_id);
		$visit_date = Carbon::parse($visit->visit_date)->format('Y-m-d');
		$revisit_date = Carbon::parse($visit->getAttribute('re-visit_date'))->format('Y-m-d');
		return view('visit-det')->with(compact('visit'))->with(compact('service'))->with(compact('visit_date'))->with(compact('revisit_date'));
	}

	public function editVisit(Request $request){
		$visit = Visit::find($request->id);
		$patients = Patient::all();
		$services = Service::all();
		$drugs = Medicine::all();
		$questions = Question::all();
		$patient = Patient::find($visit->patient_id);

		$date = Carbon::parse($patient->dob);
		$age = $date->diff(Carbon::today());


		$years = $age->y;
		$months = $age->m;
		$days = $age->d;

		$dob = Carbon::parse($patient->dob)->format('Y-m-d');


		$consangunity = $patient->Consangunity;
		$visit_date = Carbon::parse($visit->visit_date)->format('Y-m-d');
		$revisit_date = Carbon::parse($visit->getAttribute('re-visit_date'))->format('Y-m-d');

		return view('edit-visit')->
			with(compact('patients'))->
			with(compact('services'))->
			with(compact('drugs'))->
			with(compact('questions'))->
			with('today', Carbon::today()->format('Y-m-d'))->
			with(compact('visit'))->
			with(compact('visit_date'))->
			with(compact('revisit_date'))->
			with(compact('patient'))->
			with(compact('years'))->
			with(compact('months'))->
			with(compact('dob'))->
			with(compact('days'));

	}


	public function getPatientVisits(Request $request)
	{
		$id = (int)$request->id;
		$visits = Visit::where('patient_id', '=', $id)->get();
		return $visits->toJson();
	}

	public function createVisit(Request $request){

		$patient = Patient::find((int)$request->patient_id);
		$patient->name = $request->patient_name;
		$patient->gender = $request->patient_gender;
		$patient->Consangunity = $request->patient_consangunity;
		$patient->dob = Carbon::parse($request->patient_dob);
		$patient->save();

		$data = array();
		$data['patient_id'] = $request->patient_id;
		$data['service_id'] = $request->service_id;
		$data['re-visit_date'] = $request->revisit_date;
		$data['visit_date'] = $request->visit_date;
		$data['questions'] = json_encode($request->questions);
		$data['medicine'] = json_encode($request->drugs);
		$data['complaint'] = $request->complaint;
		$data['investigation'] = $request->investigation;
		$data['prevhistory'] = $request->prevhistory;

		Visit::create($data);

		echo "visit saved";



	}

	public function saveEditedVisit(Request $request){

		$visit = Visit::find($request->id);


		$visit->service_id = $request->service_id;
		$visit->setAttribute('re-visit_date', $request->revisit_date);
		$visit->visit_date = $request->visit_date;
		$visit->investigation = $request->investigation;
		$visit->prevhistory = $request->prevhistory;
		$visit->complaint = $request->complaint;
		$visit->questions = json_encode($request->questions);
		$visit->medicine = json_encode($request->drugs);

		$visit->save();


		$visit = Visit::find($request->id);

		echo "visit edited";

	}

}
