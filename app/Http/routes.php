<?php
use \clinic\Patient;
use \clinic\Service;
use \clinic\Visit;
use \clinic\User;
use \Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/login', array('as'=>'getlogin', 'uses' => 'MyAuth@getLogin'));
Route::post('/login', array('as'=>'postlogin', 'uses' => 'MyAuth@postLogin'));



Route::group(array('middleware' => 'auth'), function () {

	Route::get('/',array('as'=>'root', 'uses' => 'IndexController@index'));
	
	Route::get('/patient',array('as'=>'getpatient', 'uses' => 'PatientController@createPatient'));

	Route::post('/patient',array('as'=>'postpatient', 'uses' => 'PatientController@updatePatient'));
	
	Route::get('/patientinfo',array('as'=>'patientinfo', 'uses' => 'PatientController@patientInfo'));



	Route::get('/calcage',array('as'=>'calcage', 'uses' => 'AgeController@index'));
	Route::get('/calcdob',array('as'=>'calcdob', 'uses' => 'AgeController@dob'));
	
	Route::get('/logout',array('as'=>'logout', 'uses' => 'MyAuth@getLogout'));

	Route::get('/index',array('as'=>'index', 'uses' => 'IndexController@index'));

	Route::get('/drugs',array('as'=>'getdrugs', 'uses' => 'MedicineController@index'));
	
	Route::put('/drugs',array('as'=>'putdrugs', 'uses' => 'MedicineController@createMedicine'));
	
	Route::delete('/drugs',array('as'=>'deletedrugs', 'uses' => 'MedicineController@deleteMedicine'));

	Route::get('/questions',array('as'=>'getquestions', 'uses' => 'QuestionController@index'));
	
	Route::put('/questions',array('as'=>'putquestions', 'uses' => 'QuestionController@createQuestion'));
	
	Route::delete('/questions',array('as'=>'deletequestions', 'uses' => 'QuestionController@deleteQuestion'));

	Route::get('/user', array('as'=>'getuser', 'uses'=>'UserController@index'));
	Route::post('/user', array('as'=>'postuser', 'uses'=>'UserController@editUser'));

	Route::get('/services',array('as'=>'getservices', 'uses' => 'ServiceController@index'));
	Route::put('/services',array('as'=>'putservices', 'uses' => 'ServiceController@createService'));
	Route::delete('/services',array('as'=>'deleteservices', 'uses' => 'ServiceController@deleteService'));

	Route::get('/history',array('as'=>'gethistory', 'uses' => 'VisitController@index'));
	Route::put('/history',array('as'=>'puthistory', 'uses' => 'VisitController@createVisit'));
	Route::get('/patientvisits',array('as'=>'getpatientvisits', 'uses' => 'VisitController@getPatientVisits'));
	
	Route::get('/history/edit/{id}',array('as'=>'geteditvisit', 'uses' => 'VisitController@editVisit'));
	Route::post('/history/edit/{id}',array('as'=>'posteditvisit', 'uses' => 'VisitController@saveEditedVisit'));
	Route::get('/history/{id}',array('as'=>'visit', 'uses' => 'VisitController@details'));
});

Route::get('/test', function () {
	// $patient_id = Patient::all()->first()->id;
	// $service_id = Service::all()->first()->id;
	// $visit_date = Carbon::today();
	// $revisit_date = Carbon::today()->subDays(5);
	// $questions = "question1";
	// $medicine = "question1";
	// $visitData = compact(array(
	// 	'patient_id', 'service_id', 'visit_date', 'questions', 'medicine'
	// 	));
	// $visitData['re-visit_date'] = $revisit_date;

	// Visit::create($visitData);

	// echo "created";

	// $credentials = array('name' => 'essam tarik', 'email' => 'essamtarik2000@gmail.com', 'password' => bcrypt('123'));

	// User::create($credentials);
	// echo "user created";

	// Auth::attempt(array('email' => "essamtarik2000@gmail.com", 'password'=>'123'));
	// var_dump(Auth::check());

	return view('testform');

});
