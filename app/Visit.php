<?php

namespace clinic;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
	protected $table = 'visits';
	protected $fillable = array( 'patient_id', 'service_id', 'visit_date', 're-visit_date', 'questions', 'complaint', 'medicine', 'investigation', 'prevhistory');
}
