<?php

namespace clinic;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	protected $table = 'questions';
	protected $fillable = array('text');

}
