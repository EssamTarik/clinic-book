<?php

namespace clinic;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
	protected $table = 'patients';
	protected $fillable = array('name', 'dob', 'Consangunity', 'gender');
}
