@extends('templates.default-without')

@section('content')

			<div class="container-fluid">
				<div class="page-content">
					<!-- BEGIN BREADCRUMBS -->
					<div class="breadcrumbs">
						<ol class="breadcrumb">
							<li>
								<a href="{{route('root')}}">Home</a>
							</li>
							<li>
								<a href="#">Settings</a>
							</li>
							<li class="active">Drugs</li>
						</ol>
					</div>
					<!-- END BREADCRUMBS -->
					<!-- BEGIN PAGE BASE CONTENT -->
					<!--start history table-->
					<div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="flaticon-pills-1
 								font-white" style="margin-left:-15px; margin bottom:10px;"></i>Drugs</div>
 								</div>

<!-------------------->
<div class="portlet-body">

                 <div class="row">
                
                @if(Session::has('info'))
                    <div class="alert alert-info">
                    {{Session::get('info')}}
                    </div>
                @endif


                @if(Session::has('error'))
                    <div class="alert alert-danger">
                    {{Session::get('error')}}
                    </div>
                @endif

                @if(count($errors))
                  <div class="alert alert-danger">
                  @foreach($errors->all() as $error)
                    {{$error}}<br>
                    @endforeach
                  </div>
                @endif
                </div>


<form action="{{route('putdrugs')}}" method="post">  

<div class="portlet light bordered">
 <form>
 <!--1st row-->
  <div class="row">
  	<div class="col-md-5">
    	<div class="form-group">
            <label class="col-md-4 control-label">Medicine Name</label>
            <div class="col-md-8">
                <div class="input-icon right">
                	
                	<input required name="name" type="text" class="form-control" placeholder="Enter Medicine">
                </div>
            </div>
        </div>
	</div>
	<!--end 1st col-->	
	<!--start 2nd col-->
      <div class="col-md-5">
        	<div class="form-group">

        <label class="col-md-3 control-label">Category</label>
        <div class="col-md-7">
            <div class="input-icon right">	
            	<input type="text" name="category" class="form-control" placeholder="Enter Category">
            </div>
        </div>
    </div>
   </div>
        <!--end 2nd col-->
        <!--start 3rd col-->
    <div class="col-md-2">
        <br>
        <label class="control-label">
        <!-- <input type="checkbox">
   		Active </label> -->
   	</div>
        <!--end 3rd col-->	
    </div>
<!--end 1st row-->
<!--Start 2nd row-->  
  <div class="row">
  	<div class="col-md-12">
		 <div class="actions"  style="padding-bottom: 10px;">
                <br>
                <div class="clearfix">
                    <button class="btn green" type="submit">Save</button>

			<input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>
    				</form>
				</div>
  		</div>
  	</div>
  </div>
   <!--end 2nd row-->
  </div>
 <!--end portlet light bordered-->

  <!--start drugs-tables-->
                               

  <div class="row">
  <div class="col-md-12">
<form action="{{route('deletedrugs')}}" method="post"> 

<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
        <thead>
            <tr>
                <th>
                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                <th> Medicine</th>
                <th> Category</th>
            </tr>
        </thead>
        <tbody>

		@foreach($drugs as $drug)
    			<tr class="odd gradeX">
    			    <td>
    			        <input name="{{$drug->id}}" type="checkbox" class="checkboxes" value="1" /> </td>
    			    <td>{{$drug->name}}</td>
    			    <td>
    			        {{$drug->category}}</a>
    			    </td>
    			</tr>
        @endforeach


        </tbody>
    </table>
<button class="btn yellow-saffron" type="submit">Delete</button>
<input type="hidden" name="_method" value="delete"/>
<input type="hidden" name="_token" value="{{csrf_token()}}"/>

</div>
</div>
                            
                            
  <!--End drugs-tables-->          
<!-- start pagination row-->
                                    
                   </div>
                </div>
                
                        
                           	
            </div>
                       
        </div>

	</div>


@stop