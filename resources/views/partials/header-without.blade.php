<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title>Clinic Book</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- BEGIN LAYOUT FIRST STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
		<!-- END LAYOUT FIRST STYLES -->
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="{{route('root')}}/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="{{route('root')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		 <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{route('root')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="{{route('root')}}/assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/layouts/layout5/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/layouts/layout5/css/custom.css" rel="stylesheet" type="text/css" />
		
		
		<!-- END THEME LAYOUT STYLES -->
		<link rel="shortcut icon" href="{{route('root')}}/favicon.ico" /> </head>
	<!-- END HEAD -->

   <body class="page-header-fixed page-sidebar-closed-hide-logo">
		<!-- BEGIN CONTAINER -->
	  <div class="wrapper">
		  <!-- BEGIN HEADER -->
		<header class="page-header">
		  <nav class="navbar mega-menu" role="navigation">
			<div class="container-fluid">
				<div class="clearfix navbar-fixed-top">
					<!-- Brand and toggle get grouped for better mobile display -->
					<!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="toggle-icon">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</span>
					</button>-->
					<!-- End Toggle Button -->
					  <!-- BEGIN LOGO -->
					  <a id="index" class="page-logo" href="{{route('root')}}">
						  <img src="{{route('root')}}/assets/global/img/custom-img/logo3.png" alt="Logo"> </a>
					  <!-- END LOGO -->
					  <!-- BEGIN SEARCH -->
					  <!--<form class="search" action="extra_search.html" method="GET">
						  <input type="name" class="form-control" name="query" placeholder="Search...">
						  <a href="javascript:;" class="btn submit">
							 <i class="fa fa-search"></i> 
						  </a>
					  </form>
				  <!-- END SEARCH -->

 <!-- BEGIN TOPBAR ACTIONS -->
	<div class="topbar-actions">
		<!-- BEGIN GROUP NOTIFICATION -->
		
								
								<div class="btn-group-red btn-group">
									<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<i class="fa fa-gear"></i>                                    </button>
									<ul class="dropdown-menu-v2" role="menu">
										<li >
											<a href="{{route('getservices')}}">add services</a>
											</li>
										<li class="">
											<a href="{{route('getdrugs')}}">add drugs </a>         </li>
										<li>
											<a href="{{route('getquestions')}}">add questions</a></li>
										
										
									</ul>
								</div>
								<!-- END GROUP INFORMATION -->
								<!-- BEGIN USER PROFILE -->
								<div class="btn-group-img btn-group">
									<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<span>Hi, {{\Auth::user()->name}}</span>
										<img onClick = "window.location.href='{{route('getuser')}}'" src="{{route('root')}}/assets/layouts/layout5/img/avatar1.jpg" alt=""> </button>
									<!--<ul class="dropdown-menu-v2" role="menu">
										<li>
											<a href="page_user_profile_1.html">
												<i class="icon-user"></i> My Profile
												<span class="badge badge-danger">1</span>                                            </a>                                        </li>
										<li>
											<a href="app_calendar.html">
												<i class="icon-calendar"></i> My Calendar </a>                                        </li>
										<li>
											<a href="app_inbox.html">
												<i class="icon-envelope-open"></i> My Inbox
												<span class="badge badge-danger"> 3 </span>                                            </a>                                        </li>
										<li>
											<a href="app_todo_2.html">
												<i class="icon-rocket"></i> My Tasks
												<span class="badge badge-success"> 7 </span>                                            </a>                                        </li>
										<li class="divider"> </li>
										<li>
											<a href="page_user_lock_1.html">
												<i class="icon-lock"></i> Lock Screen </a>                                        </li>
										<li>
											<a href="page_user_login_1.html">
												<i class="icon-key"></i> Log Out </a>                                        </li>
									<!--</ul>-->
								</div>
								<!-- END USER PROFILE -->
								<!-- BEGIN QUICK SIDEBAR TOGGLER -->
								<button onClick = "location.href = '{{route('logout')}}'" type="button" class="quick-sidebar-toggler"data-toggle="collapse">
									<span class="sr-only">Toggle Quick Sidebar</span>
									<i class="icon-logout"></i>                                </button>
								<!-- END QUICK SIDEBAR TOGGLER -->
							</div>
						  <!-- END TOPBAR ACTIONS -->
						</div>
						<!-- BEGIN HEADER MENU -->
						<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
						<!--#centeredmenu is used to center tabs-->
		<!--<div id="centeredmenu">
		<ul class="nav navbar-nav">
			<li class="dropdown dropdown-fw ">
			<a href="index.html" class="text-uppercase">
			<i class="flaticon-stethoscope-1">
				</i> new </a>
			</li>
			<li class="dropdown dropdown-fw">
			<a href="history.html" class="text-uppercase">
				<i class="flaticon-medical-1">
				</i> history </a>
			</li>
			
		</ul>
		</div>
						</div>
						<!-- END HEADER MENU -->
					<!--</div>
					<!--/container-->
				</nav>
			</header>-->
