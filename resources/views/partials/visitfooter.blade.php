
<!--end page content--->
		<!-- BEGIN FOOTER -->
			<div class="copyright">
			<p>2016 © Clinic Book by Bridge online Solutions . </p>
			</div>                        
			<a href="#index" class="go2top">
			<i class="icon-arrow-up"></i>
			</a>
				<!-- END FOOTER -->
			</div>
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!-- END QUICK SIDEBAR -->
		<!--[if lt IE 9]>
<script src="../{{url('/assets')}}/global/plugins/respond.min.js"></script>
<script src="../{{url('/assets')}}/global/plugins/excanvas.min.js"></script> 
<![endif]-->
		<!-- BEGIN CORE PLUGINS -->
<script src="{{url('/assets')}}/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{url('/assets')}}/jquery.maskedinput.js" type="text/javascript"></script>

        <script src="{{url('/assets')}}/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
        
        <script src="{{url('/assets')}}/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
        <script src="{{url('/assets')}}/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
        
        
        <script src="{{url('/assets')}}/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

        
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="{{url('/assets')}}/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="{{url('/assets')}}/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{url('/assets')}}/pages/scripts/form-validation.min.js" type="text/javascript"></script>
         <script src="{{url('/assets')}}/pages/scripts/components-select2.min.js" type="text/javascript"></script>

        
        <!-- END PAGE LEVEL SCRIPTS -->
        
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="{{url('/assets')}}/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
		<script src="{{url('/assets')}}/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
		<!-- END THEME LAYOUT SCRIPTS -->
		<!--start calender-->
		<div name="datepicker"class="datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top" style="top: 2977px; left: 686px; z-index: 10; display: none;"><div class="datepicker-days" style="display: block;"><table class=" table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="fa fa-angle-left"></i></th><th colspan="5" class="datepicker-switch">September 2016</th><th class="next" style="visibility: visible;"><i class="fa fa-angle-right"></i></th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="old day">28</td><td class="old day">29</td><td class="old day">30</td><td class="old day">31</td><td class="day">1</td><td class="day">2</td><td class="day">3</td></tr><tr><td class="day">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td><td class="day">10</td></tr><tr><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td><td class="day">17</td></tr><tr><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td><td class="day">24</td></tr><tr><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td><td class="new day">1</td></tr><tr><td class="new day">2</td><td class="new day">3</td><td class="new day">4</td><td class="new day">5</td><td class="new day">6</td><td class="new day">7</td><td class="new day">8</td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="fa fa-angle-left"></i></th><th colspan="5" class="datepicker-switch">2016</th><th class="next" style="visibility: visible;"><i class="fa fa-angle-right"></i></th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month">Nov</span><span class="month">Dec</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="fa fa-angle-left"></i></th><th colspan="5" class="datepicker-switch">2010-2019</th><th class="next" style="visibility: visible;"><i class="fa fa-angle-right"></i></th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year">2019</span><span class="year new">2020</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div></div>
		<!--end calender-->
	</body>

</html>