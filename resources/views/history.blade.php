<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title>Clinic Book</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- BEGIN LAYOUT FIRST STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
		<!-- END LAYOUT FIRST STYLES -->
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="{{route('root')}}/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="{{route('root')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{route('root')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->

		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="{{route('root')}}/assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/layouts/layout5/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{route('root')}}/assets/global/css/bootstrap-select.css" type="text/css" >
		<link rel="stylesheet" href="{{route('root')}}/assets/global/css/select2.css" type="text/css" >
		<link href="{{route('root')}}/assets/layouts/layout5/css/custom.css" rel="stylesheet" type="text/css" />

		
		
		<!-- END THEME LAYOUT STYLES -->
		<link rel="shortcut icon" href="{{route('root')}}/favicon.ico" /> </head>
	<!-- END HEAD -->

   <body class="page-header-fixed page-sidebar-closed-hide-logo">
		<!-- BEGIN CONTAINER -->
	  <div class="wrapper">
		  <!-- BEGIN HEADER -->
		<header class="page-header">
		  <nav class="navbar mega-menu" role="navigation">
			<div class="container-fluid">
				<div class="clearfix navbar-fixed-top">
					<!-- Brand and toggle get grouped for better mobile display -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="toggle-icon">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</span>
					</button>
					<!-- End Toggle Button -->
					  <!-- BEGIN LOGO -->
					  <a id="index" class="page-logo" href="{{route('root')}}">
						  <img src="{{route('root')}}/assets/global/img/custom-img/logo3.png" alt="Logo"> </a>
					  <!-- END LOGO -->
					  <!-- BEGIN SEARCH -->
					<!--  <form class="search" action="extra_search" method="GET">
						  <input type="name" class="form-control" name="query" placeholder="Search...">
						  <a href="javascript:;" class="btn submit">
							 <i class="fa fa-search"></i> 
						  </a>
					  </form>-->
				  <!-- END SEARCH -->

 <!-- BEGIN TOPBAR ACTIONS -->
	<div class="topbar-actions">
		<!-- BEGIN GROUP NOTIFICATION -->
		
								<!-- END GROUP NOTIFICATION -->
								<!-- BEGIN GROUP INFORMATION -->
								<!--settings menu---------------->
								<div class="btn-group-red btn-group">
									<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<i class="fa fa-gear"></i>                                    </button>
									<ul class="dropdown-menu-v2" role="menu">
										<li >
											<a href="{{route('getservices')}}">add services</a>
											</li>
										<li>
											<a href="{{route('getdrugs')}}">add drugs </a>         </li>
										<li>
											<a href="{{route('getquestions')}}">add questions</a></li>
										
										
									</ul>
								</div>
								<!-- END GROUP INFORMATION -->
								<!-- BEGIN USER PROFILE -->
								<div class="btn-group-img btn-group">
									<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<span>Hi, {{\Auth::user()->name}}</span>
										<img onClick = "window.location.href='{{route('getuser')}}'" src="{{route('root')}}/assets/layouts/layout5/img/avatar1.jpg" alt=""> </button>

								</div>
								<!-- END USER PROFILE -->
								<!-- BEGIN QUICK SIDEBAR TOGGLER -->
								<button onClick = "location.href = '{{route('logout')}}'" type="button" class="quick-sidebar-toggler" data-toggle="collapse">
									<span class="sr-only">Toggle Quick Sidebar</span>
									<i class="icon-logout"></i>                                </button>
								<!-- END QUICK SIDEBAR TOGGLER -->
							</div>
						  <!-- END TOPBAR ACTIONS -->
						</div>
						<!-- BEGIN HEADER MENU -->
						<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
							<!--<div id="center-tabs">-->
							<div id="centeredmenu"><ul class="nav navbar-nav">
								<li class="dropdown dropdown-fw ">
								<a href="{{route('root')}}" class="text-uppercase">
								<i class="flaticon-stethoscope-1">
									</i> new </a>
								</li>
								<li class="dropdown dropdown-fw open">
								<a href="{{route('gethistory')}}" class="text-uppercase">
									<i class="flaticon-medical-1">
									</i> history </a>
								</li>
								
							</ul>
							</div>
							<!--</div>-->
				   </div>
						<!-- END HEADER MENU -->
					</div>
					<!--/container-->
				</nav>
			</header>
			<!-- END HEADER -->
			<div class="container-fluid">
				<div class="page-content">
					<!-- BEGIN BREADCRUMBS -->
					<div class="breadcrumbs">
						<ol class="breadcrumb">
							<li>
								<a href="">Home</a>
							</li>
							<li class="active">History</li>
						</ol>
					</div>
					<!-- END BREADCRUMBS -->
					<!-- BEGIN PAGE BASE CONTENT -->
					<!--start history table-->
					<div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user" style="font-size: 18px;"></i>Patients History 
                                    </div>                
                                </div>
                                <div class="portlet-body">		


							<div class="portlet light bordered">

							<div class="row">
										<div class="col-md-3">						
										<label class="control-label col-md-4">Name:
										</label>
										<div id="patientname" class="data-txt col-md-8">loading</div>
									</div>
									<div class="col-md-3">
										<label class="control-label col-md-4">Gender:
										</label>
										<div id="gender" class="data-txt col-md-8">loading</div>
									</div>	
									<div class="col-md-4">
										<label class="control-label col-md-3">Age:
										</label>
										<div id="age" class="data-txt col-md-9">loading</div>
									</div>
									<div class="col-md-2">
										<label class="control-label col-md-8">Consangunity:
										</label>
										<div class="data-txt col-md-4">
										<i id="consangunity" class="fa fa-check" style=";margin-top: 2px; margin-left: 15px;">
										</i></div>
									</div>
								</div>
 							</div>

							 <!--1st row-->

						<div class="row">
							<div class="col-md-12">
							 	<div class="portlet light bordered">
							 	<div class="portlet-title">
									<div class="caption">
									<h4>Patient History</h3>
									</div>
						        </div>
						    	<div class="table-responsive">
							 	<table id="mydatatable" class="table table-bordered table-hover table-checkable order-column" id="sample_1">
							        <thead style="background-color: rgba(69, 182, 175, 0.3);">
							            <tr>
							                <th>
							                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
							                <th> Visit Date</th>
							                <th> Diagnosis</th>
							                <th> treatment</th>
							                <th> Action</th>

							            </tr>
							        </thead>
							        <tbody id="visitstable">


								    </tbody>
								    </table>
								    </div>
								 	
								</div>
							</div>
						</div>	
                    </div>                       
                </div>
        	</div>

				
            
@include('partials.footer')

<script type="text/javascript">
$("document").ready(function(){

	function calcAge(dob){
		var request = $.ajax({
		  url: "{{route('calcage')}}",
		  type: "GET",
		  data: {date : dob},
		  dataType: "html"
		});

		request.done(function(msg) {
			var age = $.parseJSON(msg);
			$("#age").text(
				age.years + " Years " +
				age.months + " Months " +
				age.days + " Days"
				);
		});

		request.fail(function(jqXHR, textStatus) {
		  console.log( "Request failed: " + textStatus );
		});
	}
	var start = document.cookie.indexOf("patient_id");
	
	if (start!=-1){
		var patient_id = document.cookie.substring(start);
		patient_id = patient_id.split(';')[0].split('=')[1].trim();
		console.log(patient_id);


		var request = $.ajax({
			url: "{{route('patientinfo')}}",
			type: "GET",
			data: {id:patient_id},
			dataType: 'json'
		});

		request.done(function(data){

			if (data.gender == 1){
				$("#gender").text('male');
			}else{
				$("#gender").text('female');
			}

			calcAge(data.dob);
			if (data.consangunity == 0){
			 	$("#consangunity").removeClass('fa-check');
			 	$("#consangunity").addClass('fa-times');
			 }
			// $("#dob").val(data.dob);
			// calcAge();
			$("#patientname").text(data.name);

			var visitsrequest = $.ajax({
				url : "{{route('getpatientvisits')}}",
				type : "GET",
				data : {id: patient_id},
				dataType : 'json'
			});
			console.log('getting visits');
			visitsrequest.done(function(visits){

				for (var i = visits.length - 1; i >= 0; i--) {
					var visit = visits[i];
					var medicine = $.parseJSON(visit.medicine);
					var drugs = []
					try{
						for (var x = medicine.length - 1; x >= 0; x--) {
						drugs[x] = Object.keys(medicine[x])[0];
					}}
					catch(e){
						console.log(e);
					}
					console.log(drugs);
					var link = "{{route('gethistory')}}/" + visit.id;
					// $("#visitstable").append('<tr class="odd gradeX"><td><input type="checkbox" class="checkboxes" value="1" /> </td><td>'+
					// 	visit.visit_date.substring(0,10) + '</td><td>'+
					// 	visit.investigation+' </a> </td><td>'+
					// 	drugs.join(', ') +'</td><td><a class="btn red" href="'+link+'"> Visit details </a></td></tr>');
					t = $("#mydatatable").DataTable();
			        t.row.add( [
			        	'<tr class="odd gradeX"><td><input type="checkbox" class="checkboxes" value="1" />',
			        		visit.visit_date.substring(0,10),
				        	visit.investigation,
				        	drugs.join(', '),
				        	'<a class="btn red" href="'+link+'"> Visit details </a>'
			        ] ).draw( false );				}

							});


		});


	}else
	{
		$("#patientname").text('no patient selected');
		$("#consangunity").text('');
		$("#gender").text('');
		$("#age").text('');
		$("#consangunity").removeClass('fa-check');
		console.log('no patient id');
	}

});

</script>