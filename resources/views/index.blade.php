<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title>Clinic Book</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- BEGIN LAYOUT FIRST STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{route('root')}}/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{route('root')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{route('root')}}/assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/layouts/layout5/css/flaticon.css" rel="stylesheet" type="text/css" />
		

		<link href="{{route('root')}}/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<link href="{{route('root')}}/assets/layouts/layout5/css/custom.css" rel="stylesheet" type="text/css" />
		
		<!-- END THEME LAYOUT STYLES -->
		<link rel="shortcut icon" href="favicon.ico" /> </head>
	<!-- END HEAD -->

   <body class="page-header-fixed page-sidebar-closed-hide-logo">
		<!-- BEGIN CONTAINER -->
	  <div class="wrapper">
		  <!-- BEGIN HEADER -->
		<header class="page-header">
		  <nav class="navbar mega-menu" role="navigation">
			<div class="container-fluid">
				<div class="clearfix navbar-fixed-top">
					<!-- Brand and toggle get grouped for better mobile display -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="toggle-icon">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</span>
					</button>
					<!-- End Toggle Button -->
					  <!-- BEGIN LOGO -->
				    <a id="index" class="page-logo" href="{{route('root')}}">
					<img src="{{route('root')}}/assets/global/img/custom-img/logo-index.png" alt="Logo"> </a>
				    <!-- END LOGO -->

 <!-- BEGIN TOPBAR ACTIONS -->
	<div class="topbar-actions">
		
		<!--settings menu-------------->
		<div class="btn-group-red btn-group">
			<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			<i class="fa fa-gear"></i>                      </button>
			<ul class="dropdown-menu-v2" role="menu">
				<li >
				<a href="{{route('getservices')}}">add services</a>
				</li>
				<li>
				<a href="{{route('getdrugs')}}">add drugs </a>         </li>
				<li>
				<a href="{{route('getquestions')}}">add questions</a>
				</li>				
			</ul>
		</div>
<!-- END Settings Menu -->
		<!-- BEGIN USER PROFILE -->
		<div class="btn-group-img btn-group">
			<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<span>Hi, {{\Auth::user()->name}}</span>
				<img onClick = "window.location.href='{{route('getuser')}}'" src="{{route('root')}}/assets/layouts/layout5/img/avatar1.jpg" alt=""> </button>
		</div>
		<!-- END USER PROFILE -->
		<!-- BEGIN QUICK SIDEBAR TOGGLER -->
		<button onClick = "location.href = '{{route('logout')}}'" type="button" class="quick-sidebar-toggler" data-toggle="collapse">
			<span class="sr-only">Toggle Quick Sidebar</span>
			<i class="icon-logout"></i>                     </button>
		<!-- END QUICK SIDEBAR TOGGLER -->
		</div>
	  <!-- END TOPBAR ACTIONS -->
	</div>
		<!-- BEGIN HEADER MENU -->
	<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
		<!--#centeredmenu is used to center tabs-->
		<div id="centeredmenu">
		<ul class="nav navbar-nav">
			<li class="dropdown dropdown-fw open">
			<a href="{{route('index')}}" class="text-uppercase">
			<i class="flaticon-stethoscope-1">
			</i> new </a>
			</li>
			<li class="dropdown dropdown-fw">
			<a href="{{route('gethistory')}}" class="text-uppercase">
			<i class="flaticon-medical-1">
			</i> history </a>
			</li>		
		</ul>
		</div>
   </div>
		<!-- END HEADER MENU -->
</div>
<!--/container-->
</nav>
</header>
<!-- END HEADER -->

<!--Start Page Container-->
	<div class="container-fluid">
		<div class="page-content">
			<!-- BEGIN BREADCRUMBS -->
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li>
						<a href="#">Home</a>
					</li>
					<li class="active">new</li>
				</ol>
			</div>
	<!-- END BREADCRUMBS -->

	<!-- BEGIN PAGE BASE CONTENT -->
				@if(Session::has('info'))
                    <div class="alert alert-info">
                    {{Session::get('info')}}
                    </div>
                @endif
				@if(Session::has('error'))
                    <div class="alert alert-danger">
                    {{Session::get('error')}}
                    </div>
                @endif

                 <div class="row">
                @if(count($errors))
                  <div class="alert alert-danger">
                  @foreach($errors->all() as $error)
                    {{$error}}<br>
                    @endforeach
                  </div>
				@endif
				</div>

	<div class="portlet light bg-inverse">
        <div class="portlet-title">
            <div class="caption">
                <i class="flaticon-heartbeat
				font-green-haze" style="font-size: 18px;"></i>
                <span class="caption-subject bold font-green-haze uppercase"> Main Information </span>
            </div>
        </div>
            <!--End title-->
        <div class="portlet-body">
        <!--start to make form 2 columns-->
		
		<form action="index" class="form-horizontal form-row-seperated">
			<!--Start Name row-->
			<div class="row">


				<div class="portlet" id="name-portlet">
					<div  id="name-first" >
					
					<div class="col-md-12">
					<label class="control-label col-md-2">
						Name
					</label>
						<div class="col-md-8" style="margin-top: 5px;">
						<select name="select1" id="patients" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="Search Names" >
							<option></option>
							@foreach($patients as $patient)
							<option value="{{$patient->id}}">{{$patient->name}}</option>
		        			@endforeach
            			</select>
							<!--<input type="text" class="form-control" placeholder="Search for Name">
							</input>-->
						</div>
						<div class="col-md-2">
							<div class="clear-fix">
							<button id="name-search" type="button" class="btn green" >
							<i class="fa fa-search"></i>    </button>
							<button id="name-add" type="button" class="btn red" data-toggle="modal" href="#large">
							<a>Add New</a>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
			<!--End portlet bordered-->
	</div>


<!---start modal-box-->
<div class="modal fade bs-modal-dialog" id="okmessage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
       <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 id="msgtxt" class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>Visit Saved</h4></center>
            </div>

            <div class="modal-footer">
                <center><button id="okbtn" type="button" class="btn green" data-dismiss="modal">Ok</button></center>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--end modal-box-->


<div class="modal fade bs-modal-dialog" id="errormessage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
       <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 id="errortxt" class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>Error!!</h4></center>
            </div>

            <div class="modal-footer">
                <center><button type="button" class="btn green" data-dismiss="modal">Ok</button></center>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--end modal-box-->


<div class="modal fade bs-modal-dialog" id="modalerrormessage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
       <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 id="modalerrortxt" class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>Error!!</h4></center>
            </div>

            <div class="modal-footer">
                <center><button id="modalerrorbtn" type="button" class="btn green" data-dismiss="modal">Ok</button></center>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade bs-modal-dialog" id="ajaxservicemodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
       <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4  class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>Add A New Service</h4></center>
            </div>
            <form>
            <div class="modal-body">
            <div class="form-group">
					  <label class="col-md-3 control-label">Service Name : </label>
					    <div class="col-md-8">
					    	<input required="required" id="ajaxservicename" type="text" class="form-control" placeholder="">
							


					    </div>
					</div>
			</div>

            <div class="modal-footer">
                <center><button id="ajaxservicebtn" type="submit" class="btn green" data-dismiss="modal">Ok</button></center>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--end modal-box-->


<div class="modal fade bs-modal-dialog" id="ajaxquestionmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
       <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>Add A New Question</h4></center>
            </div>
            <form>
            <div class="modal-body">
            <div class="form-group">
					  <label class="col-md-3 control-label">Question : </label>
					    <div class="col-md-8">
					    	<input required="required" id="ajaxquestionname" type="text" class="form-control" placeholder="">
							


					    </div>
					</div>
			</div>

            <div class="modal-footer">
                <center><button id="ajaxquestionbtn" type="submit" class="btn green" data-dismiss="modal">Ok</button></center>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--end modal-box-->


<div class="modal fade bs-modal-dialog" id="ajaxdrugmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
       <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>Add A New Drug</h4></center>
            </div>
            <form>
            <div class="modal-body">
            <div class="form-group">
					  <label class="col-md-3 control-label">Drug : </label>
					    <div class="col-md-8">
					    	<input required="required" id="ajaxdrugname" type="text" class="form-control" placeholder="">
							


					    </div>
					</div>
			</div>

            <div class="modal-footer">
                <center><button id="ajaxdrugbtn" type="submit" class="btn green" data-dismiss="modal">Ok</button></center>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<!---start modal-box-->
<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
       <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>New Patient</h4>
            </div>
            <div class="modal-body">
        		<div class="portlet light bg-inverse">
        			<div class="row">
	<div class="col-md-4">
		<!--Name formgroup-->
	<div class="form-group">
	  <label class="col-md-3 control-label">Name</label>
	    <div class="col-md-9">
	    <input id="name-modal" required="true" type="text" class="form-control" placeholder="Enter Name">
		



	    </div>
	</div>
<!--end name formgroup-->
<!--Gender formgroup-->
<div class="form-group form-md-radios has-success">
	 <label class="col-md-3 control-label" for="form_control_1">Gender</label>
        <div class="col-md-9">
            <div class="md-radio-inline">
                <div class="md-radio">
                    <input type="radio" id="checkbox1_6" name="radio2" class="md-radiobtn">
                    <label for="checkbox1_6">
                    <span class="inc"></span>
                    <span class="check"></span>
                    <span class="box"></span> Male </label>
                </div>
                <div class="md-radio">
                    <input type="radio" id="checkbox1_7" name="radio2" class="md-radiobtn" checked="">
                    <label for="checkbox1_7">
                    <span></span>
                    <span class="check"></span>
                    <span class="box">	
                    </span> Female </label>
                </div>
	        </div>
        </div>
	</div>
<!--end gender formgroup-->
</div>
<!--start 2nd col-->
<div class="col-md-4">
<!--start consangunity field-->
	<div class="form-group ">
 	<label class="col-md-7 control-label">Consangunity
 	</label>
 		<div class="col-md-1">                           
			<div class="md-checkbox">
                <input type="checkbox" id="checkbox33" class="md-check" name="check2">
                <label for="checkbox33">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>  </label>
			</div>
		</div>
	</div>	 

</div>
<!--end 2nd col-->
<!--start 3rd col-->
	<div class="col-md-4">
	
    <!---start date of birth---->
	<div class="form-group">
	<label class="control-label col-md-3">D.O.B</label>
        <div class="col-md-9">
            <div class="input-group date" data-date-format="yyyy-mm-dd">
                <input id="dob-modal" required="true" type="text" class="form-control"  name="datepicker">
                    <span class="input-group-btn">
                        <button class="btn default" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
            	</div>
		<!-- /input-group -->
		</div>
	</div>	
	<!--end date of birth-->
	<!--start age field-->
					<div class="form-group">
						<label class="control-label col-md-3">Age</label>
						<div class="col-md-9">
							<div class="clearfix calcdob-modal">
							<input id="year-modal" type="text" name="year" class="form-control " placeholder="year" style="width: 30%; float: left;">
							<input id="month-modal" type="text" name="month" class="form-control " placeholder="Month" style="width: 30% ; float: left; margin-left: 3px;">
							<input id="day-modal" type="text" name="day" class="form-control " placeholder="Day" style="width: 25% ; float: left; margin-left: 3px;">
							</div>			 			
						</div>
					</div>
					<!--end age field-->	
				</div>
				<!--end 3rd col-->
			</div>
			<!--end row-->
		</div>
	</div>
            <div class="modal-footer">
                <button type="button" class="btn green" data-dismiss="modal">Close</button>
                <button id="btn-modal" type="button" class="btn red">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--end modal-box-->
<!-------------------start 2nd row-------------------->
<div class="row">
<div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold font-green-haze uppercase">Current Patient : </span><hr>
            </div>
</div>

	<div class="col-md-4">
		<!--Name formgroup-->
	<div class="form-group">
	  <label class="col-md-3 control-label">Name</label>
	    <div class="col-md-8">
	    	<input id="name" type="text" class="form-control" placeholder="">
			</input>

	    <!-- <select  name="select1"id="single" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="Name">
			<option></option>
            <option value="Ayman Gomaa">Ayman Gomaa</option>
	        <option value="Ahmad Ramadan">Ahmad Ramadan
	        </option>
	        <option value="Amr Nabil">Amr Nabil</option>
	        <option value="kareem Mohammad">kareem Mohammad
	        </option>
            </select>-->
	    </div>
	</div>
<!--end name formgroup-->
<!--Gender formgroup-->
<div class="form-group form-md-radios has-success">
	 <label class="col-md-3 control-label" for="form_control_1">Gender</label>
        <div class="col-md-9">
            <div class="md-radio-inline">
                <div class="md-radio">
                    <input type="radio" id="checkbox1_8" name="radio1" class="md-radiobtn">
                    <label for="checkbox1_8">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span> Male </label>
                </div>
                <div class="md-radio">
                    <input type="radio" id="checkbox1_9" name="radio1" class="md-radiobtn" checked="">
                    <label for="checkbox1_9">
                    <span></span>
                    <span class="check"></span>
                    <span class="box">	
                    </span> Female </label>
                </div>
	        </div>
        </div>
	</div>
<!--end gender formgroup-->
</div>
<!--start 2nd col-->
<div class="col-md-4">
<!--start consangunity field-->
	<div class="form-group ">
 	<label class="col-md-7 control-label">Consangunity
 	</label>
 		<div class="col-md-1">                           
			<div class="md-checkbox">
                <input type="checkbox" id="checkbox34" class="md-check" name="check1">
                <label for="checkbox34">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>  </label>
			</div>
		</div>
	</div>	 

</div>
<!--end 2nd col-->
<!--start 3rd col-->
	<div class="col-md-4">
	
    <!---start date of birth---->
	<div class="form-group">
	<label class="control-label col-md-3">D.O.B</label>
        <div class="col-md-9">
            <div class="input-group date" data-date-format="yyyy-mm-dd">
                <input id="dob" type="text" class="form-control"  name="datepicker">
                    <span class="input-group-btn">
                        <button class="btn default" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
            </div>
		<!-- /input-group -->
		</div>
	</div>	
	<!--end date of birth-->
	<!--start age field-->
	<div class="form-group">
		<label class="control-label col-md-3">Age</label>
			<div class="col-md-9">
				<div class="clearfix calcdob">
					<input id="year" type="text" name="year" class="form-control " placeholder="year" style="width: 30%; float: left; margin-left: 0px;" >
					<input id="month" type="text" name="month" class="form-control " placeholder="Month" style="width: 30% ; float: left; margin-left: 3px;" >
					<input id="day" type="text" name="day" class="form-control " placeholder="Day"style="width: 25% ; float: left; margin-left: 3px;">
				</div>			 			
			</div>
		</div>
	<!--end age field-->
	
	</div>
	<!--end 3rd col-->
</div>
<!--end row-->
<!-------------------end 2nd row---------------------->
</div>             <hr>
							<center><button style="text-align: right;" id="updatepatient" type="button" class="btn red" data-toggle="modal">
							<a>Update Patient</a>
							</button></center>
</div>
<!--end portlet body-->

<div class="portlet light bg-inverse" style="padding-bottom: 20px;">
	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical
			font-green-haze bold" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Visit </span>


		</div>

			<div style="text-align:right;"><button style="text-align: right;" id="quickaddservicebtn" type="button" class="btn red" data-toggle="modal">
							<a>add new service</a>
							</button></div>
	</div>
<!--End title-->
<div class="portlet-body">    
<!--start row-->
<div class="row ">
<!--start 1st col-->


								
<!--start complaint-->
<div class="col-md-4">
<!--start service field-->
 <div class="form-group ">
	<label class="col-md-3 control-label">Services</label>
        <div class="col-md-8">
            <div class="input-icon right">
            	<select id="service" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="select a service">
    			<option></option>
    			@foreach($services as $service)
    			<option value="{{$service->id}}">{{$service->name}}</option>
                @endforeach
                </select> 
            </div>
    	</div>
	</div>
    <!--end service field-->
</div>
<!--end 1st column-->
					


<div class="col-md-4">
<!---start visit field---->
<div class="form-group">
<label class="control-label col-md-2">Visit</label>
<div class="col-md-9">
<div class="input-group date" data-date-format="yyyy-mm-dd">
<input value = "{{$today}}" id="visit" type="text" class="form-control"  name="datepicker">
<span class="input-group-btn">
<button class="btn default" type="button">
<i class="fa fa-calendar"></i>
</button>
</span>
</div>
<!-- /input-group -->
</div>
</div>
<!--end visit field-->
</div>
<!--end 2nd col-->
<div class="col-md-4">
<!---start visit field---->
		<div class="form-group">
			<label class="control-label col-md-3">Re-visit</label>
			<div class="col-md-9">
			<div class="input-group date" data-date-format="yyyy-mm-dd">
			<input value = "{{$week}}" id="revisit" type="text" class="form-control"  name="datepicker">
			<span class="input-group-btn">
			<button class="btn default" type="button">
			<i class="fa fa-calendar"></i>
			</button>
			</span>
			</div>
		<!-- /input-group -->
		 </div>
		</div>
<!--end visit formgroup-->
	</div>
<!--end col-->				
</div> 
<!--end row-->
</div>
<!--end portlet body-->
</div>


<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical-1
			font-green-haze" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Previous History </span>
		</div>
	</div>
<!--End title-->
	<div class="portlet-body">    
<!--start row-->
		<div class="row ">
<!--start 1st col-->
			<div class="col-md-12">
<!--start complaint-->
				<div class="form-group">
				<textarea id="prevhistory" class="form-control" rows="3"></textarea>
			</div>
		</div> 
	<!--end row-->
	</div>
<!--end portlet body-->
</div>
<!--end portlet-->	
     	
<!-- end row-->
</div>

<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical-1
			font-green-haze" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Complaint </span>
		</div>
	</div>
<!--End title-->
	<div class="portlet-body">    
<!--start row-->
		<div class="row ">
<!--start 1st col-->
			<div class="col-md-12">
<!--start complaint-->
				<div class="form-group">
				<textarea id="complaint" class="form-control" rows="3"></textarea>
			</div>
		</div> 
	<!--end row-->
	</div>
<!--end portlet body-->
</div>
<!--end portlet-->	
     	
<!-- end row-->
</div>
    <!--start questions portlet-->

 	<div class="portlet light bg-inverse">
		<div class="portlet-title">
			<div class="caption">
            <i class="icon-question font-green-haze" style="font-size: 20px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> Questions </span>
            </div>
			<div style="text-align:right;"><button style="text-align: right;" id="quickaddquestionbtn" type="button" class="btn red" data-toggle="modal">
			<a>add new question</a>
			</button></div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
                    <label class="col-md-3 control-label">Question</label>
	                    <div class="col-md-9">            
	                	<select id="question" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="Select a Question">
	                			<option></option>
	                			@foreach($questions as $question)
	                			<option value="{{$question->id}}">{{$question->text}}</option>
	                			@endforeach
	                            </select>        
	                    </div>
	
					</div>
					<!--end question formgroup-->
				</div>
				<!--end 1st col-->
				<div class="col-md-6">
					<div class="form-group">
                        <label class="col-md-3 control-label">Answer</label>
                        <div class="col-md-9">
                                    
    				 	<div class="clearfix">
        				<input id="answer" type="text" class="form-control text-clfix" placeholder="Enter Answer">

    					<button id="addquestion" type="submit" class="btn green" style="float: left;" ><i class="fa fa-plus"></i></button>
	            				
        				</div>
	            				
					</div>
    			</div> 
	    	</div>
	    	<!--end 2nd col-->
        </div>                                  
        <!--end row-->
<!--start row-->
		<div class="row">
			<div class="col-md-12">
				<div class="white">
	            <div class="table-responsive">
		        <table class="table">
		            <thead>
		                <tr>
		                    <th> Question  </th>
		                    <th> Answer</th>
		                    
		                </tr>
		            </thead>
		            <tbody id="questiontable">
		            </tbody>
		                </table>
		                </div>
		               <!--end div class"table-responsive"-->
						</div>
						<!--end div class"white"-->
					</div>
					<!--end col-->
				</div>
			<!--end row-->                	
			</div>	   
		    <!-- end portlet body-->	 
 		</div>
<!--end questions portlet body-->

<!--start Investigation portlet-->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
            <i class="flaticon-thermometer font-green-haze" style="font-size: 18px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> Investigation
             </span>
    	</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
                
                <textarea id="investigation" class="form-control " rows="3"></textarea>
        		</div>
         <!--end form group-->	
  			</div>
        <!--end col-->
    	</div>
    	<!--end row-->   
	</div>
<!--end portlet body-->
</div>
<!--end portlet-->

<!--Start Treatment Section-->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
            <i class="flaticon-medical-report font-green-haze" style="font-size: 18px; margin-left: -15px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> treatment
            </span>
    	</div>

			<div style="text-align:right;"><button style="text-align: right;" id="quickadddrugbtn" type="button" class="btn red" data-toggle="modal">
			<a>add new drug</a>
			</button></div>
	</div>
<div class="portlet-body">       			
	<div class="row">
		 <div class="col-md-6">
		 <div class="form-group">
			<label class="control-label col-md-3">Medicine</label>
			<div class="col-md-9">
			<select id="medicine" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="Select a Medicine">
			<option></option>
			@foreach($drugs as $drug)
			<option value="{{$drug->id}}">{{$drug->name}}</option>
            @endforeach
            </select> 
			</div>
		</div>
		</div>
		<div class="col-md-6">
		<div class="form-group">		
	 	<label class="control-label col-md-3" >Dosage</label>
			<div class="col-md-9">
	 		<div class="clearfix">
			<input id="dosage" type="text" class="form-control text-clfix" placeholder="Enter Dosage" >	      	
			<button id="addmedicine" type="submit" class="btn green" style="float: left;" >
			<i class="fa fa-plus"></i></button>
			</div>
			</div>
 		</div>
	</div>
 </div>
<!---->
		<div class="row">
			<div class="col-md-12">
			
				<div class="white">
				    <div class="table-responsive">
					<table class="table">
				    <thead>
				        <tr>
				            <th> Medicine  </th>
				            <th> Dosage</th>
				        </tr>
				    </thead>
				    <tbody id="medicinetable">
			        </tbody>
				    </table>
				</div>
			
		</div>
	</div>
</div>
                                                         
</div>
<!--end portlet body-->

</div>
<!--end portlet-->
        
<!--start main actions of the page-->
	<div class="row">
		<div class="col-md-12">
			<div class="actions pull-right">       
	    		<button id="savebtn" type="submit" class="btn red" style="margin-right: 10px;">Save
	    		</button>
	    		<button type="submit" class="btn green" style="margin-right: 10px;">cancel
	    		</button>
			</div> 
		</div>
	</div>
</div>
<!--end main actions of the page-->

</div>
<!--end  main portlet-->
</div>  	
</div>
</div>
</div>
</form>

                
@include('partials.indexfooter')

<script>

$('document').ready(function(){

	function ajaxservice(){
		var data = {

			name : $("#ajaxservicename").val(),
			_method : 'PUT',
			_token : "{{csrf_token()}}"

		};

		request = $.ajax({
			url : '{{route("putservices")}}',
			type : 'POST',
			data : data,
			dataType : 'html'
		});
		request.done(function(id){
			var option = '<option value="'+id+'">'+$("#ajaxservicename").val()+'</option>';
			$("#service").append(option);

			$("#ajaxservicemodal").modal('hide');
		});
	}

	function quickaddservice(){
		$("#ajaxservicename").val('');
		$("#ajaxservicemodal").modal('show');
	}

	$("#quickaddservicebtn").click(function(){
		quickaddservice();
	});

	$("#ajaxservicebtn").click(function(){
		if($("#ajaxservicename").val() != "")
			ajaxservice();
	});



	function ajaxquestion(){
		var data = {

			text : $("#ajaxquestionname").val(),
			_method : 'PUT',
			_token : "{{csrf_token()}}"

		};

		request = $.ajax({
			url : '{{route("putquestions")}}',
			type : 'POST',
			data : data,
			dataType : 'html'
		});

		request.done(function(id){
			var option = '<option value="'+id+'">'+$("#ajaxquestionname").val()+'</option>';
			$("#question").append(option);

			$("#ajaxquestionmodal").modal('hide');
		});
	}

	function quickaddquestion(){
		$("#ajaxquestionname").val('');
		$("#ajaxquestionmodal").modal('show');
	}

	$("#quickaddquestionbtn").click(function(){
		quickaddquestion();
	});

	$("#ajaxquestionbtn").click(function(){
		if($("#ajaxquestionname").val() != "")
			ajaxquestion();
	});




	function ajaxdrug(){
		var data = {

			name : $("#ajaxdrugname").val(),
			_method : 'PUT',
			_token : "{{csrf_token()}}"

		};

		request = $.ajax({
			url : '{{route("putdrugs")}}',
			type : 'POST',
			data : data,
			dataType : 'html'
		});
		request.done(function(id){
			var option = '<option value="'+id+'">'+$("#ajaxdrugname").val()+'</option>';
			$("#medicine").append(option);

			$("#ajaxdrugmodal").modal('hide');
		});
	}

	function quickadddrug(){
		$("#ajaxdrugname").val('');
		$("#ajaxdrugmodal").modal('show');
	}

	$("#quickadddrugbtn").click(function(){
		quickadddrug();
	});

	$("#ajaxdrugbtn").click(function(){
		if($("#ajaxdrugname").val() != "")
			ajaxdrug();
	});




	var start = document.cookie.indexOf("patient_id");
	var patient_id;
	if (start!=-1){
		patient_id = document.cookie.substring(start);
		patient_id = patient_id.split(';')[0].split('=')[1].trim();

		var request = $.ajax({
			url: "{{route('patientinfo')}}",
			type: "GET",
			data: {id:patient_id},
			dataType: 'json'
		});

		request.done(function(data){

			if (data.gender == 1){
				$("#checkbox1_8").prop('checked', true);
			}else{
				$("#checkbox1_9").prop('checked', true);
			}

			if (data.consangunity == 1){
				$("#checkbox34").prop('checked', true);
			}else{
				$("#checkbox34").prop('checked', false);
			}

			$("#patients").val(patient_id);
			console.log(data.name)
			$("#dob").val(data.dob);
			calcAge();
			$("#name").val(data.name);

			});
	}


	$("#updatepatient").click(function(){
		
		if ($("#patients").val() == ""){
			showError('please select a patient');
			return;
		}

		if ($("#name").val().trim() == ""){
			showError('please enter a new name for the patient');
			return;
		}

		if ($("#dob").val() == ""){
			showError('please enter a new birthdate for the patient');
			return;
		}

		var data = {

			patient_id : $("#patients").val(),
			patient_name : $("#name").val().trim(),
			patient_gender : 0,
			patient_consangunity : 0,
			patient_dob : $("#dob").val(),

			service_id : $("#service").val(),
			visit_date : $("#visit").val(),
			revisit_date : $("#revisit").val(),

			complaint : $("#complaint").val(),
			investigation : $("#investigation").val(),

			questions : questions,
			drugs : drugs
		};

		data['_token'] = "{{csrf_token()}}";

		if ($("#checkbox1_8").is(':checked')){
			data.patient_gender = 1;
		}

		if ($("#checkbox34").is(':checked')){
			data.patient_consangunity = 1;
		}

		
		var request = $.ajax({
			url : '{{route("postpatient")}}',
			type : "POST",
			data : data,
			dataType : 'html'
		});

		request.done(function(msg){
			showError(msg);
		});



function savefunc(){
		var data = {

			patient_id : $("#patients").val(),
			patient_name : $("#name").val().trim(),
			patient_gender : 0,
			patient_consangunity : 0,
			patient_dob : $("#dob").val(),

			service_id : $("#service").val(),
			visit_date : $("#visit").val(),
			revisit_date : $("#revisit").val(),

			complaint : $("#complaint").val(),
			investigation : $("#investigation").val(),

			questions : questions,
			drugs : drugs
		};

		data['_token'] = "{{csrf_token()}}";
		data['_method'] = "put";

		if ($("#checkbox1_8").is(':checked')){
			data.patient_gender = 1;
		}

		if ($("#checkbox34").is(':checked')){
			data.patient_consangunity = 1;
		}

		
		var request = $.ajax({
			url : '{{route("puthistory")}}',
			type : "POST",
			data : data,
			dataType : 'html'
		});

		request.done(function(msg){
			$("#okmessage").modal('show');
		});
	}
	});
	$('.date').datepicker();
	$('#visit').mask('9999-99-99');
	$('#revisit').mask('9999-99-99');

	$('#dob').mask('9999-99-99');
	$('#dob-modal').mask('9999-99-99');

	var questions = [];
	var drugs = [];
	function calcAge(){
		var request = $.ajax({
		  url: "{{route('calcage')}}",
		  type: "GET",
		  data: {date : $('#dob').val()},
		  dataType: "html"
		});

		request.done(function(msg) {
			var age = $.parseJSON(msg);
			$("#year").val(age.years);
			$("#month").val(age.months);
			$("#day").val(age.days);
		});

		request.fail(function(jqXHR, textStatus) {
		  console.log( "Request failed: " + textStatus );
		});
	}
	$('#dob').change(function(){
		calcAge();
		
	});

	$('.calcdob').change(function(){


		days = $('#day').val();
		if (days=='')
			days = '0'

		months = $('#month').val();
		if (months=='')
			months = '0'
		
		years = $('#year').val();
		if (years=='')
			years = '0'
		

		var request = $.ajax({
		  url: "{{route('calcdob')}}",
		  type: "GET",
		  data: {year:years, month:months, day:days},
		  dataType: "json"
		});

		request.done(function(msg) {
			var age = msg;
			$("#dob").val(age.year+'-'+age.month+'-'+age.day)

		});

		request.fail(function(jqXHR, textStatus) {
		  console.log( "Request failed: " + textStatus );
		});
	});





	$('#dob-modal').change(function(){

		var request = $.ajax({
		  url: "{{route('calcage')}}",
		  type: "GET",
		  data: {date : $('#dob-modal').val()},
		  dataType: "html"
		});

		request.done(function(msg) {
			var age = $.parseJSON(msg);
			$("#year-modal").val(age.years);
			$("#month-modal").val(age.months);
			$("#day-modal").val(age.days);
		});

		request.fail(function(jqXHR, textStatus) {
		  console.log( "Request failed: " + textStatus );
		});
	});

	$('.calcdob-modal').change(function(){


		days = $('#day-modal').val();
		if (days=='')
			days = '0'

		months = $('#month-modal').val();
		if (months=='')
			months = '0'
		
		years = $('#year-modal').val();
		if (years=='')
			years = '0'
		

		var request = $.ajax({
		  url: "{{route('calcdob')}}",
		  type: "GET",
		  data: {year:years, month:months, day:days},
		  dataType: "json"
		});

		request.done(function(msg) {
			var age = msg;
			$("#dob-modal").val(age.year+'-'+age.month+'-'+age.day)


		});

		request.fail(function(jqXHR, textStatus) {
		  console.log( "Request failed: " + textStatus );
		});
	});

	$("#modalerrormessage").on('hide.bs.modal', function(){
		if($("#modalerrortxt").text()=="New Patient Created")
			window.location.href="{{route('root')}}";
		else
			$("#large").modal('show');

	})
	$("#btn-modal").click(function(){
		var cons = 0;
		if($("#checkbox33").is(':checked'))
			cons=1;

		var gender = 0;
		if($("#checkbox1_6").is(':checked'))
			gender=1;
		
		if ($("#name-modal").val()==""){
			$("#large").modal('hide');
			modalShowError('name field is empty');
			return;
		}

		if ($("#dob-modal").val()==""){
			$("#large").modal('hide');
			modalShowError('birthdate field is empty');

			return;
		}
		data = {
			name: $("#name-modal").val(),
			Consangunity: cons,
			dob: $("#dob-modal").val(),
			gender: gender
		};

		var request = $.ajax({
			url: "{{route('getpatient')}}",
			type: "GET",
			data: data,
			dataType: 'html'
		});

		request.done(function(msg){
			$("#large").modal('hide');
			modalShowError(msg);
		});

	});
	
	function readPatient(id){
		var request = $.ajax({
			url: "{{route('patientinfo')}}",
			type: "GET",
			data: {id:id},
			dataType: 'json'
		});

		request.done(function(data){

			if (data.gender == 1){
				$("#checkbox1_8").prop('checked', true);
			}else{
				$("#checkbox1_9").prop('checked', false);
			}

			if (data.consangunity == 1){
				$("#checkbox34").prop('checked', true);
			}else{
				$("#checkbox34").prop('checked', false);
			}

			$("#dob").val(data.dob);
			calcAge();
			$("#name").val(data.name);


		});
	}

	$("#patients").change(function(){
		if ($("#patients").val()!=""){
			document.cookie = "patient_id = " + $("#patients").val();
			location.href="{{route('gethistory')}}";
		}
	});


	$("#addmedicine").click(function(){
		var row= '<tr><td>' + $("#medicine :selected").text() + "</td><td>"+$("#dosage").val()+"</td></tr>";

		$("#medicinetable").append(row);

		var medicinedata = {};
		medicinedata[$("#medicine :selected").text().trim()] = $("#dosage").val().trim();
		drugs.push(medicinedata);


	});


	$("#addquestion").click(function(){
		var row= '<tr><td>' + $("#question :selected").text() + "</td><td>"+$("#answer").val()+"</td></tr>";
		$("#questiontable").append(row);
		
		var questiondata = {};
		questiondata[$("#question :selected").text().trim()] = $("#answer").val().trim();
		questions.push(questiondata);

	});


function showError(txt){
	$("#errortxt").text(txt)
	$("#errormessage").modal('show');
}


function modalShowError(txt){
	$("#modalerrortxt").text(txt);
	$("#modalerrormessage").modal('show');

}


function savefunc(){
		var data = {

			patient_id : $("#patients").val(),
			patient_name : $("#name").val().trim(),
			patient_gender : 0,
			patient_consangunity : 0,
			patient_dob : $("#dob").val(),

			service_id : $("#service").val(),
			visit_date : $("#visit").val(),
			revisit_date : $("#revisit").val(),

			complaint : $("#complaint").val(),
			investigation : $("#investigation").val(),
			prevhistory : $("#prevhistory").val(),

			questions : questions,
			drugs : drugs
		};

		data['_token'] = "{{csrf_token()}}";
		data['_method'] = "put";

		if ($("#checkbox1_8").is(':checked')){
			data.patient_gender = 1;
		}

		if ($("#checkbox34").is(':checked')){
			data.patient_consangunity = 1;
		}

		
		var request = $.ajax({
			url : '{{route("puthistory")}}',
			type : "POST",
			data : data,
			dataType : 'html'
		});

		request.done(function(msg){
			$("#okmessage").modal('show');
		});
	}


	$('#okmessage').on('hidden.bs.modal', function () {
		window.location.href="{{route('root')}}"
	});

	$("#savebtn").click(function(){



		if ($("#patients").val() == ""){
			showError('please select a patient');
			return;
		}

		if ($("#name").val().trim() == ""){
			showError('please enter a new name for the patient');
			return;
		}

		if ($("#dob").val() == ""){
			showError('please enter a new birthdate for the patient');
			return;
		}

		if ($("#service").val() == ""){
			showError('please choose a service');
			return;
		}


		if ($("#complaint").val() == ""){
			showError('please set a complaint');
			return;
		}

		if ($("#investigation").val() == ""){
			showError('please set an investigation');
			return;
		}

		savefunc();
	});

});

</script>