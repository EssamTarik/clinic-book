@extends('templates.default-without')

@section('content')

			<div class="container-fluid">
				<div class="page-content">
					<!-- BEGIN BREADCRUMBS -->
					<div class="breadcrumbs">
						<ol class="breadcrumb">
							<li>
								<a href="{{route('root')}}">Home</a>
							</li>
							<li class="active">User</li>
						</ol>
					</div>
					<!-- END BREADCRUMBS -->
					<!-- BEGIN PAGE BASE CONTENT -->
					<!--start history table-->
					<div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="flaticon-pills-1
 								font-white" style="margin-left:-15px; margin bottom:10px;"></i>Edit User</div>
 								</div>

<!-------------------->
<div class="portlet-body">

                 <div class="row">
                
                @if(Session::has('info'))
                    <div class="alert alert-info">
                    {{Session::get('info')}}
                    </div>
                @endif


                @if(Session::has('error'))
                    <div class="alert alert-danger">
                    {{Session::get('error')}}
                    </div>
                @endif

                @if(count($errors))
                  <div class="alert alert-danger">
                  @foreach($errors->all() as $error)
                    {{$error}}<br>
                    @endforeach
                  </div>
                @endif
                </div>


<form action="{{route('postuser')}}" method="post" enctype="multipart/form-data">  

<div class="portlet light bordered">
 <!--1st row-->
  <div class="row">
  	<div class="col-md-5">
    	<div class="form-group">
            <label class="col-md-4 control-label">Email </label>
            <div class="col-md-8">
                <div class="input-icon right">
                	
                	<input required name="email" type="email" class="form-control" placeholder="Enter Email" value="{{\Auth::user()->email}}">
                </div>
            </div>
        </div>
	</div>
	<!--end 1st col-->	
	<!--start 2nd col-->
      <div class="col-md-5">
        	<div class="form-group">

        <label class="col-md-3 control-label">Name </label>
        <div class="col-md-7">
            <div class="input-icon right">	
            	<input required type="text" name="name" class="form-control" placeholder="Enter Name" value="{{\Auth::user()->name}}">
            </div>
        </div>
    </div>
   </div>
        <!--end 2nd col-->
        <!--start 3rd col-->
    <div class="col-md-2">
        <br>
        <label class="control-label">
        <!-- <input type="checkbox">
   		Active </label> -->
   	</div>
        <!--end 3rd col-->	
    </div>

      <div class="row">
  	<div class="col-md-5">
    	<div class="form-group">
            <label class="col-md-4 control-label">Password </label>
            <div class="col-md-8">
                <div class="input-icon right">
                	
                	<input name="password" type="password" class="form-control" placeholder="Enter password">
                </div>
            </div>
        </div>
	</div>
	<!--end 1st col-->	
	<!--start 2nd col-->
      <div class="col-md-5">
        	<div class="form-group">

        <label class="col-md-3 control-label">Confirm </label>
        <div class="col-md-7">
            <div class="input-icon right">	
            	<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
            </div>
        </div>
    </div>
   </div>
        <!--end 2nd col-->
        <!--start 3rd col-->
    <div class="col-md-2">
        <br>
        <label class="control-label">
        <!-- <input type="checkbox">
   		Active </label> -->
   	</div>
        <!--end 3rd col-->	
    </div>

          <div class="row">
  	<div class="col-md-5">
    	<div class="form-group">
            <label class="col-md-4 control-label">Picture </label>
            <div class="col-md-8">
                <div class="input-icon right">
                	
                	<input name="picture" type="file" class="form-control">
                </div>
            </div>
        </div>
	</div>
	<!--end 1st col-->	
	<!--start 2nd col-->
        <!--end 2nd col-->
        <!--start 3rd col-->
    <div class="col-md-2">
        <br>
        <label class="control-label">
        <!-- <input type="checkbox">
   		Active </label> -->
   	</div>
        <!--end 3rd col-->	
    </div>
<!--end 1st row-->
<!--Start 2nd row-->  
  <div class="row">
  	<div class="col-md-12">
		 <div class="actions"  style="padding-bottom10px;">
                <br>
                <div class="clearfix">
                    <button class="btn green" type="submit">Save</button>

			<input type="hidden" name="_token" value="{{csrf_token()}}"/>
</form>
				</div>
  		</div>
  	</div>
  </div>
   <!--end 2nd row-->
  </div>
 <!--end portlet light bordered-->

  <!--start drugs-tables-->
                               


@stop