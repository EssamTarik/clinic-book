@extends('templates.default-without')

@section('content')

			<div class="container-fluid">
				<div class="page-content">
					<!-- BEGIN BREADCRUMBS -->
					<div class="breadcrumbs">
						<ol class="breadcrumb">
							<li>
								<a href="{{route('root')}}">Home</a>
							</li>
							<li>
								<a href="#">Settings</a>
							</li>
							<li class="active">Services</li>
						</ol>
					</div>
					<!-- END BREADCRUMBS -->
					<!-- BEGIN PAGE BASE CONTENT -->
					<!--start history table-->
					<div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-medkit
								 font-white" 
								 style="font-size: 18px;"></i>Services </div>
<!---->

<!---->


                                <div class="tools"> </div>
                                </div>
                                
                                <div class="portlet-body">
                                
                                    <div id="sample_2_wrapper" class="dataTables_wrapper no-footer"><div class="row"><div class="col-md-12"><div class="dt-buttons">
                                      </div>
                                     </div>
                                    </div>

<!---->
                 <div class="row">
                
                @if(Session::has('info'))
                    <div class="alert alert-info">
                    {{Session::get('info')}}
                    </div>
                @endif


                @if(Session::has('error'))
                    <div class="alert alert-danger">
                    {{Session::get('error')}}
                    </div>
                @endif

                @if(count($errors))
                  <div class="alert alert-danger">
                  @foreach($errors->all() as $error)
                    {{$error}}<br>
                    @endforeach
                  </div>
                @endif
                </div>
<div class="portlet light bordered">
	<form action="{{route('putservices')}}" method="post">  
	<div class="row">
  	<div class="col-md-5">
    	<div class="form-group">
            <label class="col-md-4 control-label">Service Name</label>
            <div class="col-md-8">
                <div class="input-icon right">	
            	<input required name="name" type="text" class="form-control" placeholder="Enter Name">
                </div>
            </div>
        </div>
	</div>
                <!--end 1st col-->	
                <!--start 2nd col-->
    <div class="col-md-5">
    	<div class="form-group">
        <label class="col-md-4 control-label">Service price</label>
                <div class="col-md-5">
                    <div class="input-icon right">	
                    	<input required name="price" type="number" step="0.01" class="form-control" min="0" placeholder="Enter price">
                    </div>
                </div>
            </div>
                </div>
                <!--end 2nd col-->
                <!--start 3rd col-->
                <div class="col-md-2">
                <br>
                <label class="control-label">
<!--                 <input type="checkbox">
           		Active </label> -->
           		</div>
                <!--end 3rd col-->	
            </div>
  	
  <div class="row">
  	<div class="col-md-12">
		<div class="form-group">
    	<label class="control-label col-md-1" >Description</label>
    	<textarea name="description" class="form-control col-md-10 " rows="2"></textarea>
		</div>
	</div>

</div>
	<!--end 1st row-->
<div class="row">
	<div class="col-md-12">
		 <div class="actions" style="padding-bottom: 10px;">
            <br>
            
            <div class="clearfix">
            <input class="btn green" type="submit" value="Save"/>
			<input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

			</form>
			</div>
		</div>
	  </div>
	</div>

</div>

  <!--end portlet container--> 

<!-- start services table-->
<div class="row">
  <div class="col-md-12">
<form action="{{route('deleteservices')}}" method="post"> 
<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
        <thead>
            <tr>
                <th>
                <input " type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                <th> Service</th>
                <th> Price</th>
            </tr>
        </thead>
        <tbody>

        @foreach($services as $service)
    			<tr class="odd gradeX">
    			    <td>
    			        <input name="{{$service->id}}" type="checkbox" class="checkboxes" value="1" /> </td>
    			    <td>{{$service->name}}</td>
    			    <td>
    			        {{$service->price}}</a>
    			    </td>
    			</tr>
        @endforeach
			    </tbody>
		    </table>	
<button class="btn yellow-saffron" type="submit">Delete</button>
<input type="hidden" name="_method" value="delete"/>
<input type="hidden" name="_token" value="{{csrf_token()}}"/>
</form>
	</div>


</div>
					<!--end portlet-->
                        
                           </form>
                           </div>	
                        </div>
                       
                        </div>

                	</div>

@stop