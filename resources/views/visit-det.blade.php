<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title>Clinic Book</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- BEGIN LAYOUT FIRST STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{route('root')}}/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{route('root')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{route('root')}}/assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/layouts/layout5/css/flaticon.css" rel="stylesheet" type="text/css" />
		

		<link href="{{route('root')}}/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<link href="{{route('root')}}/assets/layouts/layout5/css/custom.css" rel="stylesheet" type="text/css" />
		
		<!-- END THEME LAYOUT STYLES -->
		<link rel="shortcut icon" href="{{route('root')}}/favicon.ico" /> </head>
	<!-- END HEAD -->

   <body class="page-header-fixed page-sidebar-closed-hide-logo">
		<!-- BEGIN CONTAINER -->
	  <div class="wrapper">
		  <!-- BEGIN HEADER -->
		<header class="page-header">
		  <nav class="navbar mega-menu" role="navigation">
			<div class="container-fluid">
				<div class="clearfix navbar-fixed-top">
					<!-- Brand and toggle get grouped for better mobile display -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="toggle-icon">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</span>
					</button>
					<!-- End Toggle Button -->
					  <!-- BEGIN LOGO -->
				    <a id="index" class="page-logo" href="{{route('root')}}">
					<img src="{{route('root')}}/assets/global/img/custom-img/logo3.png" alt="Logo"> </a>
				    <!-- END LOGO -->

 <!-- BEGIN TOPBAR ACTIONS -->
	<div class="topbar-actions">
		
		<!--settings menu-------------->
		<div class="btn-group-red btn-group">
			<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			<i class="fa fa-gear"></i>                      </button>
			<ul class="dropdown-menu-v2" role="menu">
				<li >
				<a href="{{route('getservices')}}">add services</a>
				</li>
				<li>
				<a href="{{route('getdrugs')}}">add drugs </a>         </li>
				<li>
				<a href="{{route('getquestions')}}">add questions</a>
				</li>				
			</ul>
		</div>
<!-- END Settings Menu -->
		<!-- BEGIN USER PROFILE -->
		<div class="btn-group-img btn-group">
			<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<span>Hi, {{\Auth::user()->name}}</span>
				<img onClick = "window.location.href='{{route('getuser')}}'" src="{{route('root')}}/assets/layouts/layout5/img/avatar1.jpg" alt=""> </button>
		</div>
		<!-- END USER PROFILE -->
		<!-- BEGIN QUICK SIDEBAR TOGGLER -->
		<button type="button" onClick = "location.href = '{{route('logout')}}'" class="quick-sidebar-toggler" data-toggle="collapse">
			<span class="sr-only">Toggle Quick Sidebar</span>
			<i class="icon-logout"></i>                     </button>
		<!-- END QUICK SIDEBAR TOGGLER -->
		</div>
	  <!-- END TOPBAR ACTIONS -->
	</div>
		<!-- BEGIN HEADER MENU -->
	<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
		<!--#centeredmenu is used to center tabs-->
		<div id="centeredmenu">
		<ul class="nav navbar-nav">
			<li class="dropdown dropdown-fw ">
			<a href="{{route('root')}}" class="text-uppercase">
			<i class="flaticon-stethoscope-1">
			</i> new </a>
			</li>
			<li class="dropdown dropdown-fw">
			<a href="{{route('gethistory')}}" class="text-uppercase">
			<i class="flaticon-medical-1">
			</i> history </a>
			</li>		
		</ul>
		</div>
   </div>
		<!-- END HEADER MENU -->
</div>
<!--/container-->
</nav>
</header>
<!-- END HEADER -->

<!--Start Page Container-->
	<div class="container-fluid">
		<div class="page-content">
			<!-- BEGIN BREADCRUMBS -->
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li>
						<a href="{{route('root')}}">Home</a>
					</li>
					<li>
						<a href="{{route('gethistory')}}">History</a>
					</li>
					<li class="active">Visit Details</li>
				</ol>
			</div>
	<!-- END BREADCRUMBS -->

	<!-- BEGIN PAGE BASE CONTENT -->
	
	<div class="portlet light bg-inverse">
	

	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical
			font-green-haze bold" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Visit </span>
		</div>
	</div>
<!--End title-->
<div class="portlet-body">    
<!--start row-->
<div class="row ">
<!--start 1st col-->
			
<!--start complaint-->
<div class="col-md-4">
<!--start service field-->
 <div class="form-group ">
	<label class="col-md-3 control-label">Services</label>
        <div class="col-md-9">
            
	    <input readonly="readonly" type="text" class="form-control" value="{{$service->name}}">
		</input>
            
    	</div>
	</div>
    <!--end service field-->
</div>
<!--end 1st column-->
					

<div class="col-md-4">
<!---start visit field---->
<div class="form-group">
<label class="control-label col-md-2">Visit</label>
<div class="col-md-9">
<!--<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">-->
<input type="text" class="form-control" readonly="" name="datepicker" value="{{$visit_date}}">
<!--<span class="input-group-btn">
<button class="btn default" type="button">
<i class="fa fa-calendar"></i>
</button>
</span>
</div>-->
<!-- /input-group -->
</div>
</div>
<!--end visit field-->
</div>
<!--end 2nd col-->
<div class="col-md-4">
<!---start visit field---->
		<div class="form-group">
			<label class="control-label col-md-3" style="padding-right: -15px;" >Re-visit</label>
			<div class="col-md-9">
			<!--<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">-->
			<input type="text" class="form-control" readonly="" name="datepicker" value="{{$revisit_date}}"> 
			<!--<span class="input-group-btn">
			<button class="btn default" type="button">
			<i class="fa fa-calendar"></i>
			</button>
			</span>
			</div>-->
		<!-- /input-group -->
		 </div>
		</div>
<!--end visit formgroup-->
	</div>
<!--end col-->				
</div> 
<!--end row-->
</div>
<!--end portlet body-->
</div>


<!--complaint portlet-->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical-1
			font-green-haze" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Previous History </span>
		</div>
	</div>
<!--End title-->
	<div class="portlet-body">    
<!--start row-->
		<div class="row ">
<!--start 1st col-->
			<div class="col-md-12">
<!--start complaint-->
				<div class="form-group">
				<label id="show">
				{{$visit->prevhistory}}
				<br><br><br></label>
				</div>
			</div> 
	<!--end col-->
		</div>
<!--end row-->
	</div>
<!--end portlet-body-->	
</div>



<!--complaint portlet-->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical-1
			font-green-haze" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Complaint </span>
		</div>
	</div>
<!--End title-->
	<div class="portlet-body">    
<!--start row-->
		<div class="row ">
<!--start 1st col-->
			<div class="col-md-12">
<!--start complaint-->
				<div class="form-group">
				<label id="show">
				{{$visit->complaint}}
				<br><br><br></label>
				</div>
			</div> 
	<!--end col-->
		</div>
<!--end row-->
	</div>
<!--end portlet-body-->	
</div>
<!--end complaint portlet-->     	

<!--start questions portlet-->

 	<div class="portlet light bg-inverse">
		<div class="portlet-title">
			<div class="caption">
            <i class="icon-question font-green-haze" style="font-size: 20px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> Questions </span>
            </div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
				<div class="form-group">
					<div class="white">
			            <div class="table-responsive">
				        <table class="table">
				            <thead>
				                <tr>
				                    <th> Question  </th>
				                    <th> Answer</th>
				                    
				                </tr>
				            </thead>
				            <tbody>
				            @if($visit->questions != "null")
				            @foreach(json_decode($visit->questions) as $question)
				                <?php 
				                	$question = (array)$question;
				                	$key = key($question);
				                	$question = (object)$question;
				                ?>
				                <tr>
				                    <td> {{$key}}</td>
				                    <td> {{$question->$key}}</td>
				                    
				                </tr>
				            @endforeach
				            @endif

				                </tbody>
				                </table>
				                </div>
				               <!--end div class"table-responsive"-->
							</div>
								<!--end div class"white"-->
							</div>
							<!--end formgroup-->
						</div>
						<!--end col-->
					</div>
					<!--end row-->                	
				</div>	   
    			<!-- end portlet body-->	 
 			</div>
			<!-- end question portlet-->	 

<!--start Investigation portlet-->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
            <i class="flaticon-thermometer font-green-haze" style="font-size: 20px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> Investigation
             </span>
    	</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
                <label id="show"> 
                {{$visit->investigation}} 
                <br>
				<br><br></label>
        		</div>
         <!--end form group-->	
  			</div>
        <!--end col-->
    	</div>
    	<!--end row-->   
	</div>
<!--end portlet body-->
</div>
<!--end portlet-->

<!--Start Treatment Section-->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
            <i class="flaticon-medical-report font-green-haze" style="font-size: 22px; margin-left: -15px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> treatment
            </span>
    	</div>
	</div>
	<div class="portlet-body">       			
		<div class="row">
			<div class="col-md-12">
			  <div class="form-group">
				<div class="white">
				    <div class="table-responsive">
					<table class="table">
				    <thead>
				        <tr>
				            <th> Medicine  </th>
				            <th> Dosage</th>
				        </tr>
				    </thead>
				    <tbody>
				    @if($visit->medicine != "null")
							@foreach(json_decode($visit->medicine) as $drug)
				                <?php 
				                	$drug = (array)$drug;
				                	$key = key($drug);
				                	$drug = (object)$drug;
				                ?>
				                <tr>
				                    <td> {{$key}}</td>
				                    <td> {{$drug->$key}}</td>
				                    
				                </tr>
				            @endforeach
				    @endif
				        </tbody>
				    </table>
					</div>
				</div>
			</div>
		</div>
	</div>                                                   
</div>
<!--end portlet body-->
	
</div>

<div style="text-align:right;">
	<a class="btn red" href='{{route("geteditvisit", $visit->id)}}'> Edit this visit </a>
</div>

<!--end treatment portlet-->
        
<!--start main actions of the page-->
	<!--<div class="row">
		<div class="col-md-12">
			<div class="actions pull-right">       
	    		<button type="submit" class="btn red" style="margin-right: 10px;">Save
	    		</button>
	    		<button type="submit" class="btn green" style="margin-right: 10px;">cancel
	    		</button>
			</div> 
		</div>
	</div>-->
<!--end main actions of the page-->

</div>
<!--end  main portlet-->
</div>  	
</div>
</div>
</div>
</form>
                


<!--end page content--->
		<!-- BEGIN FOOTER -->
			<div class="copyright">
			<p>2016 © Clinic Book by Bridge online Solutions . </p>
			</div>                        
			<a href="#index" class="go2top">
			<i class="icon-arrow-up"></i>
			</a>
				<!-- END FOOTER -->
			</div>
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!-- END QUICK SIDEBAR -->
		<!--[if lt IE 9]>
<script src="..{{route('root')}}/assets/global/plugins/respond.min.js"></script>
<script src="..{{route('root')}}/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
		<!-- BEGIN CORE PLUGINS -->
<script src="{{route('root')}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
        
        <script src="{{route('root')}}/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
        
        
        <script src="{{route('root')}}/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

        
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="{{route('root')}}/assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{route('root')}}/assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
         <script src="{{route('root')}}/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>

        
        <!-- END PAGE LEVEL SCRIPTS -->
        
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="{{route('root')}}/assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
		<script src="{{route('root')}}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
		<!-- END THEME LAYOUT SCRIPTS -->
		<!--start calender-->
		<div name="datepicker"class="datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top" style="top: 2977px; left: 686px; z-index: 10; display: none;"><div class="datepicker-days" style="display: block;"><table class=" table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="fa fa-angle-left"></i></th><th colspan="5" class="datepicker-switch">September 2016</th><th class="next" style="visibility: visible;"><i class="fa fa-angle-right"></i></th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="old day">28</td><td class="old day">29</td><td class="old day">30</td><td class="old day">31</td><td class="day">1</td><td class="day">2</td><td class="day">3</td></tr><tr><td class="day">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td><td class="day">10</td></tr><tr><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td><td class="day">17</td></tr><tr><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td><td class="day">24</td></tr><tr><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td><td class="new day">1</td></tr><tr><td class="new day">2</td><td class="new day">3</td><td class="new day">4</td><td class="new day">5</td><td class="new day">6</td><td class="new day">7</td><td class="new day">8</td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="fa fa-angle-left"></i></th><th colspan="5" class="datepicker-switch">2016</th><th class="next" style="visibility: visible;"><i class="fa fa-angle-right"></i></th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month">Nov</span><span class="month">Dec</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="fa fa-angle-left"></i></th><th colspan="5" class="datepicker-switch">2010-2019</th><th class="next" style="visibility: visible;"><i class="fa fa-angle-right"></i></th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year">2019</span><span class="year new">2020</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div></div>
		<!--end calender-->
	</body>

</html>