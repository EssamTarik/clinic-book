<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Clinic Book | Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{route('root')}}/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{route('root')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{route('root')}}/assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/layouts/layout5/css/custom.css" rel="stylesheet" type="text/css" />

        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{route('root')}}/favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
        <!-----------login-portlet------------------------------>
            <div class="login-container">
        <!--main portlet-->
                <div class="portlet light bordered">
                 <!--title-->
                    <div class="portlet-title">
      
                        <div class="caption font-green-sharp">
                        <img src="{{route('root')}}/assets/global/img/custom-img/logo-login.png">    
                        </div>
                     </div>
                 <!--end title--> 
                @if(Session::has('info'))
                    <div class="alert alert-danger">
                    {{Session::get('info')}}
                    </div>
                @endif
                 <div class="row">
                @if(count($errors))
                  <div class="alert alert-danger">
                  @foreach($errors->all() as $error)
                    {{$error}}<br>
                    @endforeach
                  </div>
                @endif

                 <form action="{{route('postlogin')}}" method="post">
                        <div class="portlet-body">
                         <div class="form-group">
                                <label class="col-md-2 control-label">email</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user font-blue"></i>
                                        </span>
                                        <input name="email" type="email" class="form-control" placeholder="Email"> 
                                     </div>
                                </div>
                          </div>
                          <div class="form-group">
                                <label class="col-md-2 control-label">password</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-lock font-blue"></i>
                                        </span>
                                        <input name="password" type="password" class="form-control" placeholder="password"> 
                                     </div>
                                </div>
                          </div>
                            <div class="form-actions">
                                <div class="btn-set center">
                                    <input type="submit" class="btn blue" value="login"/>
                                   
                                </div>
                               
                            </div>
                            <input name="_token" type="hidden" value="{{csrf_token()}}"/>      
                            </div>         
                                 </form>    
                     <!--end form-->   
 						 </div>
                     </div>
                
                        
<div class="copyright">
       <p>2016 © Clinic Book by Bridge online Solutions . </p>
        </div>                        
        <!--[if lt IE 9]>
<script src="{{route('root')}}/assets/global/plugins/respond.min.js"></script>
<script src="{{route('root')}}/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{route('root')}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{route('root')}}/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{route('root')}}/assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
        <script src="{{route('root')}}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>