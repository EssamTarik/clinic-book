<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title>Clinic Book</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- BEGIN LAYOUT FIRST STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{route('root')}}/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{route('root')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{route('root')}}/assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="{{route('root')}}/assets/layouts/layout5/css/flaticon.css" rel="stylesheet" type="text/css" />
		

		<link href="{{route('root')}}/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{route('root')}}/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<link href="{{route('root')}}/assets/layouts/layout5/css/custom.css" rel="stylesheet" type="text/css" />
		
		<!-- END THEME LAYOUT STYLES -->
		<link rel="shortcut icon" href="{{route('root')}}/favicon.ico" /> </head>
	<!-- END HEAD -->

   <body class="page-header-fixed page-sidebar-closed-hide-logo">
		<!-- BEGIN CONTAINER -->
	  <div class="wrapper">
		  <!-- BEGIN HEADER -->
		<header class="page-header">
		  <nav class="navbar mega-menu" role="navigation">
			<div class="container-fluid">
				<div class="clearfix navbar-fixed-top">
					<!-- Brand and toggle get grouped for better mobile display -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="toggle-icon">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</span>
					</button>
					<!-- End Toggle Button -->
					  <!-- BEGIN LOGO -->
				    <a id="index" class="page-logo" href="{{route('root')}}">
					<img src="{{route('root')}}/assets/global/img/custom-img/logo-index.png" alt="Logo"> </a>
				    <!-- END LOGO -->

 <!-- BEGIN TOPBAR ACTIONS -->
	<div class="topbar-actions">
		
		<!--settings menu-------------->
		<div class="btn-group-red btn-group">
			<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			<i class="fa fa-gear"></i>                      </button>
			<ul class="dropdown-menu-v2" role="menu">
				<li >
				<a href="{{route('getservices')}}">add services</a>
				</li>
				<li>
				<a href="{{route('getdrugs')}}">add drugs </a>         </li>
				<li>
				<a href="{{route('getquestions')}}">add questions</a>
				</li>				
			</ul>
		</div>
<!-- END Settings Menu -->
		<!-- BEGIN USER PROFILE -->
		<div class="btn-group-img btn-group">
			<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<span>Hi, {{\Auth::user()->name}}</span>
				<img src="{{route('root')}}/assets/layouts/layout5/img/avatar1.jpg" alt=""> </button>
		</div>
		<!-- END USER PROFILE -->
		<!-- BEGIN QUICK SIDEBAR TOGGLER -->
		<button onClick = "location.href = '{{route('logout')}}'" type="button" class="quick-sidebar-toggler" data-toggle="collapse">
			<span class="sr-only">Toggle Quick Sidebar</span>
			<i class="icon-logout"></i>                     </button>
		<!-- END QUICK SIDEBAR TOGGLER -->
		</div>
	  <!-- END TOPBAR ACTIONS -->
	</div>
		<!-- BEGIN HEADER MENU -->
	<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
		<!--#centeredmenu is used to center tabs-->
		<div id="centeredmenu">
		<ul class="nav navbar-nav">
			<li class="dropdown dropdown-fw open">
			<a href="" class="text-uppercase">
			<i class="flaticon-stethoscope-1">
			</i> edit </a>
			</li>
			<li class="dropdown dropdown-fw ">
			<a href="{{route('root')}}" class="text-uppercase">
			<i class="flaticon-stethoscope-1">
			</i> new </a>
			</li>
			<li class="dropdown dropdown-fw">
			<a href="{{route('gethistory')}}" class="text-uppercase">
			<i class="flaticon-medical-1">
			</i> history </a>
			</li>		
		</ul>
		</div>
   </div>
		<!-- END HEADER MENU -->
</div>
<!--/container-->
</nav>
</header>
<!-- END HEADER -->

<!--Start Page Container-->
	<div class="container-fluid">
		<div class="page-content">
			<!-- BEGIN BREADCRUMBS -->
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li>
						<a href="{{route('root')}}">Home</a>
					</li>
					<li>
						<a href="{{route('gethistory')}}">History</a>
					</li>
					<li>
						<a href='{{route("visit",$visit->id)}}'>Visit Details</a>
					</li>
					<li class="active">Edit</li>
				</ol>
			</div>	<!-- END BREADCRUMBS -->

	<!-- BEGIN PAGE BASE CONTENT -->
				@if(Session::has('info'))
                    <div class="alert alert-info">
                    {{Session::get('info')}}
                    </div>
                @endif
                 <div class="row">
                @if(count($errors))
                  <div class="alert alert-danger">
                  @foreach($errors->all() as $error)
                    {{$error}}<br>
                    @endforeach
                  </div>
				@endif
				</div>

	<div class="portlet light bg-inverse">
        <div class="portlet-title">
            <div class="caption">
                <i class="flaticon-heartbeat
				font-green-haze" style="font-size: 18px;"></i>
                <span class="caption-subject bold font-green-haze uppercase"> Main Information </span>
            </div>
        </div>
            <!--End title-->
        <div class="portlet-body">
        <!--start to make form 2 columns-->
		
		<div class="form-horizontal form-row-seperated">
			<!--Start Name row-->
			<div class="row">

				<div class="portlet" id="name-portlet">
					<div  id="name-first" >
					
					<div class="col-md-12">
					<label class="control-label col-md-2">
						Name
					</label>
						<div class="col-md-8" style="margin-top: 5px;">
						<select name="select1" id="patients" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="Search Names" >
							<option value="{{$patient->id}}">{{$patient->name}}</option>
							
            			</select>
							<!--<input type="text" class="form-control" placeholder="Search for Name">
							</input>-->
						</div>
					<!-- 	<div class="col-md-2">
							<div class="clear-fix">
							<button id="name-search" type="button" class="btn green" >
							<i class="fa fa-search"></i>    </button>
							<button id="name-add"type="button" class="btn red" data-toggle="modal" href="#large">
							<a>Add New</a>
							</button>
						</div>
					</div> -->
				</div>
			</div>
		</div>
			<!--End portlet bordered-->
	</div>

<!-------------------start 2nd row-------------------->
<div class="row">
	<div class="col-md-4">
		<!--Name formgroup-->
	<div class="form-group">
	  <label class="col-md-3 control-label">Name</label>
	    <div class="col-md-8">
	    	<input id="name" type="text" class="form-control" placeholder=""  readonly="readonly" value="{{$patient->name}}">
			</input>

	    <!-- <select  name="select1"id="single" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="Name">
			<option></option>
            <option value="Ayman Gomaa">Ayman Gomaa</option>
	        <option value="Ahmad Ramadan">Ahmad Ramadan
	        </option>
	        <option value="Amr Nabil">Amr Nabil</option>
	        <option value="kareem Mohammad">kareem Mohammad
	        </option>
            </select>-->
	    </div>
	</div>
<!--end name formgroup-->
<!--Gender formgroup-->
<div class="form-group form-md-radios has-success">
	 <label class="col-md-3 control-label" for="form_control_1">Gender</label>
        <div class="col-md-9">
            <div class="md-radio-inline">
                <div class="md-radio">
                	@if($patient->gender == "1")
                    <input type="radio" id="checkbox1_8" name="radio1" class="md-radiobtn" checked="true">
                    @else
                    <input disabled type="radio" id="checkbox1_8" name="radio1" class="md-radiobtn">
                    @endif
                    <label for="checkbox1_8">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span> Male </label>
                </div>
                <div class="md-radio">
                @if($patient->gender == "0")
                    <input type="radio" id="checkbox1_9" name="radio1" class="md-radiobtn" checked="">
                @else
                    <input disabled type="radio" id="checkbox1_9" name="radio1" class="md-radiobtn">
                @endif
                    <label for="checkbox1_9">
                    <span></span>
                    <span class="check"></span>
                    <span class="box">	
                    </span> Female </label>
                </div>
	        </div>
        </div>
	</div>
<!--end gender formgroup-->
</div>
<!--start 2nd col-->
<div class="col-md-4">
<!--start consangunity field-->
	<div class="form-group ">
 	<label class="col-md-7 control-label">Consangunity
 	</label>
 		<div class="col-md-1">                           
			<div class="md-checkbox">
                @if($patient->Consangunity == "1")
                <input readonly="" type="checkbox" id="checkbox34" class="md-check" name="check1" checked="true">
                @else
                <input type="checkbox" id="checkbox34" class="md-check" name="check1">
                @endif
                <label for="checkbox34">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>  </label>
			</div>
		</div>
	</div>	 

</div>
<!--end 2nd col-->
<!--start 3rd col-->
	<div class="col-md-4">
	
    <!---start date of birth---->
	<div class="form-group">
	<label class="control-label col-md-3">D.O.B</label>
        <div class="col-md-9">
            <div class="input-group" data-date-format="yyyy-mm-dd">
                <input id="dob" type="text" class="form-control" readonly="" value="{{$dob}}" name="datepicker">
                    
            </div>
		<!-- /input-group -->
		</div>
	</div>	
	<!--end date of birth-->
	<!--start age field-->
	<div class="form-group">
		<label class="control-label col-md-3">Age</label>
			<div class="col-md-9">
				<div class="clearfix calcdob">
					<input readonly="" id="year" type="text" name="year" class="form-control " placeholder="year" value="{{$years}}" style="width: 30%; float: left; margin-left: 0px;" >
					<input readonly="" id="month" type="text" value="{{$months}}" name="month" class="form-control " placeholder="Month" style="width: 30% ; float: left; margin-left: 3px;" >
					<input id="day" readonly="" type="text" name="day" value="{{$days}}" class="form-control " placeholder="Day"style="width: 25% ; float: left; margin-left: 3px;">
				</div>			 			
			</div>
		</div>
	<!--end age field-->
	
	</div>
	<!--end 3rd col-->
</div>
<!--end row-->
<!-------------------end 2nd row---------------------->
</div>             
</div>
<!--end portlet body-->

<div class="portlet light bg-inverse" style="padding-bottom: 20px;">
	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical
			font-green-haze bold" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Visit </span>
		</div>
	</div>
<!--End title-->
<div class="portlet-body">    
<!--start row-->
<div class="row ">
<!--start 1st col-->
			
<!--start complaint-->
<div class="col-md-4">
<!--start service field-->
 <div class="form-group ">
	<label class="col-md-3 control-label">Services</label>
        <div class="col-md-8">
            <div class="input-icon right">
            	<select id="service" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="select a service">
    			<option></option>
    			@foreach($services as $service)
    			@if($service->id == $visit->service_id)
    			<option selected="selected" value="{{$service->id}}">{{$service->name}}</option>
    			@else
    			<option value="{{$service->id}}">{{$service->name}}</option>
    			@endif
                @endforeach
                </select> 
            </div>
    	</div>
	</div>
    <!--end service field-->
</div>
<!--end 1st column-->
					

<div class="col-md-4">
<!---start visit field---->
<div class="form-group">
<label class="control-label col-md-2">Visit</label>
<div class="col-md-9">
<div class="input-group date" data-date-format="yyyy-mm-dd">
<input value = "{{$visit_date}}" id="visit" type="text" class="form-control"  name="datepicker">
<span class="input-group-btn">
<button class="btn default" type="button">
<i class="fa fa-calendar"></i>
</button>
</span>
</div>
<!-- /input-group -->
</div>
</div>
<!--end visit field-->
</div>
<!--end 2nd col-->
<div class="col-md-4">
<!---start visit field---->
		<div class="form-group">
			<label class="control-label col-md-3">Re-visit</label>
			<div class="col-md-9">
			<div class="input-group date" data-date-format="yyyy-mm-dd">
			<input value = "{{$revisit_date}}" id="revisit" type="text" class="form-control" name="datepicker">
			<span class="input-group-btn">
			<button class="btn default" type="button">
			<i class="fa fa-calendar"></i>
			</button>
			</span>
			</div>
		<!-- /input-group -->
		 </div>
		</div>
<!--end visit formgroup-->
	</div>
<!--end col-->				
</div> 
<!--end row-->
</div>
<!--end portlet body-->
</div>


<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical-1
			font-green-haze" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Previous History </span>
		</div>
	</div>
<!--End title-->
	<div class="portlet-body">    
<!--start row-->
		<div class="row ">
<!--start 1st col-->
			<div class="col-md-12">
<!--start complaint-->
				<div class="form-group">
				<textarea id="prevhistory" class="form-control" rows="3">{{$visit->prevhistory}}</textarea>
			</div>
		</div> 
	<!--end row-->
	</div>
<!--end portlet body-->
</div>
<!--end portlet-->	
     	
<!-- end row-->
</div>


<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="flaticon-medical-1
			font-green-haze" style="font-size: 18px; "></i>
			<span class="caption-subject bold font-green-haze uppercase"> Complaint </span>
		</div>
	</div>
<!--End title-->
	<div class="portlet-body">    
<!--start row-->
		<div class="row ">
<!--start 1st col-->
			<div class="col-md-12">
<!--start complaint-->
				<div class="form-group">
				<textarea id="complaint" class="form-control" rows="3">{{$visit->complaint}}</textarea>
			</div>
		</div> 
	<!--end row-->
	</div>
<!--end portlet body-->
</div>
<!--end portlet-->	
     	
<!-- end row-->
</div>
    <!--start questions portlet-->

 	<div class="portlet light bg-inverse">
		<div class="portlet-title">
			<div class="caption">
            <i class="icon-question font-green-haze" style="font-size: 20px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> Questions </span>
            </div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
                    <label class="col-md-3 control-label">Question</label>
	                    <div class="col-md-9">            
	                	<select id="question" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="Select a Question">
	                			<option></option>
	                			@foreach($questions as $question)
	                			<option value="{{$question->id}}">{{$question->text}}</option>
	                			@endforeach
	                            </select>        
	                    </div>
					</div>
					<!--end question formgroup-->
				</div>
				<!--end 1st col-->
				<div class="col-md-6">
					<div class="form-group">
                        <label class="col-md-3 control-label">Answer</label>
                        <div class="col-md-9">
                                    
    				 	<div class="clearfix">
        				<input id="answer" type="text" class="form-control text-clfix" placeholder="Enter Answer">

    					<button id="addquestion" type="submit" class="btn green" style="float: left;" ><i class="fa fa-plus"></i></button>
	            				
        				</div>
	            				
					</div>
    			</div> 
	    	</div>
	    	<!--end 2nd col-->
        </div>                                  
        <!--end row-->
<!--start row-->
		<div class="row">
			<div class="col-md-12">
				<div class="white">
	            <div class="table-responsive">
		        <table class="table">
		            <thead>
		                <tr>
		                    <th> Question  </th>
		                    <th> Answer</th>
		                    <th> action</th>
		                    
		                </tr>
		            </thead>
		            <tbody id="questiontable">
		            @if($visit->questions != "null")
				            @foreach(json_decode($visit->questions) as $question)
				                <?php 
				                	$question = (array)$question;
				                	$key = key($question);
									$question = (object)$question;

				                ?>
				                <tr>
				                    <td> {{$key}}</td>
				                    <td> {{$question->$key}}</td>
				                    <td><a class="btn red" 
				                    onClick = "$(this).parent().parent().remove()"> remove</a></td>
				                    
				                </tr>
				            @endforeach
					@endif

		            </tbody>
		                </table>
		                </div>
		               <!--end div class"table-responsive"-->
						</div>
						<!--end div class"white"-->
					</div>
					<!--end col-->
				</div>
			<!--end row-->                	
			</div>	   
		    <!-- end portlet body-->	 
 		</div>
<!--end questions portlet body-->

<!--start Investigation portlet-->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
            <i class="flaticon-thermometer font-green-haze" style="font-size: 18px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> Investigation
             </span>
    	</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
                
                <textarea id="investigation" class="form-control " rows="3">{{$visit->investigation}}</textarea>
        		</div>
         <!--end form group-->	
  			</div>
        <!--end col-->
    	</div>
    	<!--end row-->   
	</div>
<!--end portlet body-->
</div>
<!--end portlet-->

<!--Start Treatment Section-->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
            <i class="flaticon-medical-report font-green-haze" style="font-size: 18px; margin-left: -15px;"></i>
            <span class="caption-subject bold font-green-haze uppercase"> treatment
            </span>
    	</div>
	</div>
<div class="portlet-body">       			
	<div class="row">
		 <div class="col-md-6">
		 <div class="form-group">
			<label class="control-label col-md-3">Medicine</label>
			<div class="col-md-9">
			<select id="medicine" class="form-control select2"  data-size="8" data-live-search="true" data-placeholder="Select a Medicine">
			<option></option>
			@foreach($drugs as $drug)
			<option value="{{$drug->id}}">{{$drug->name}}</option>
            @endforeach
            </select> 
			</div>
		</div>
		</div>
		<div class="col-md-6">
		<div class="form-group">		
	 	<label class="control-label col-md-3" >Dosage</label>
			<div class="col-md-9">
	 		<div class="clearfix">
			<input id="dosage" type="text" class="form-control text-clfix" placeholder="Enter Dosage" >	      	
			<button id="addmedicine" type="submit" class="btn green" style="float: left;" >
			<i class="fa fa-plus"></i></button>
			</div>
			</div>
 		</div>
	</div>
 </div>
<!---->
		<div class="row">
			<div class="col-md-12">
			
				<div class="white">
				    <div class="table-responsive">
					<table class="table">
				    <thead>
				        <tr>
				            <th> Medicine  </th>
				            <th> Dosage</th>
				            <th> action</th>
				        </tr>
				    </thead>
				    <tbody id="medicinetable">
				    @if($visit->medicine != "null")
							@foreach(json_decode($visit->medicine) as $drug)
				                <?php 
				                	$drug = (array)$drug;
				                	$key = key($drug);
									$drug = (object)$drug;
				                ?>
				                <tr>
				                    <td> {{$key}}</td>
				                    <td> {{$drug->$key}}</td>
				                    <td><a class="btn red" 
				                    onClick = "$(this).parent().parent().remove()"> remove</a></td>

				                </tr>
				            @endforeach
				    @endif
			        </tbody>
				    </table>
				</div>
			
		</div>
	</div>
</div>
                                                         
</div>
<!--end portlet body-->

</div>
<!--end portlet-->
        
<!--start main actions of the page-->
	<div class="row">
		<div class="col-md-12">
			<div class="actions pull-right">       
	    		<button id="savebtn" type="submit" class="btn red" style="margin-right: 10px;">Save
	    		</button>
	    		<a href='{{route("visit",$visit->id)}}'>
	    		<button type="submit" class="btn green" style="margin-right: 10px;">cancel
	    		</button>
	    		</a>
			</div> 
		</div>
	</div>
</div>
<!--end main actions of the page-->

</div>
<!--end  main portlet-->
</div>  	
</div>
</div>
</div>
      

<div class="modal fade bs-modal-dialog" id="errormessage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
       <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 id="errortxt" class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>Error!!</h4></center>
            </div>

            <div class="modal-footer">
                <center><button type="button" class="btn green" data-dismiss="modal">Ok</button></center>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--end modal-box-->



<!---start modal-box-->
<div class="modal fade bs-modal-dialog" id="okmessage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
       <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 id="msgtxt" class="modal-title  bold font-green-haze"><i style=" font-size:22px; margin-top: 3px; margin-right: 3px;" class="flaticon-medicine"></i>Visit Saved</h4></center>
            </div>

            <div class="modal-footer">
                <center><button id="okbtn" type="button" class="btn green" data-dismiss="modal">Ok</button></center>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--end modal-box-->



@include('partials.visitfooter')

<script>

$('document').ready(function(){

	
	function showError(txt){
		$("#errortxt").text(txt)
		$("#errormessage").modal('show');
	}


	$('.date').datepicker();
	$('#visit').mask('9999-99-99');
	$('#revisit').mask('9999-99-99');

	$('#dob').mask('9999-99-99');
	$('#dob-modal').mask('9999-99-99');


	var questions = [];
	var drugs = [];
	

	$('.calcdob').change(function(){


		days = $('#day').val();
		if (days=='')
			days = '0'

		months = $('#month').val();
		if (months=='')
			months = '0'
		
		years = $('#year').val();
		if (years=='')
			years = '0'
		

		var request = $.ajax({
		  url: "{{route('calcdob')}}",
		  type: "GET",
		  data: {year:years, month:months, day:days},
		  dataType: "json"
		});

		request.done(function(msg) {
			var age = msg;
			$("#dob").val(age.day+'-'+age.month+'-'+age.year)

		});

		request.fail(function(jqXHR, textStatus) {
		  console.log( "Request failed: " + textStatus );
		});
	});




	
	function readPatient(id){
		var request = $.ajax({
			url: "{{route('patientinfo')}}",
			type: "GET",
			data: {id:id},
			dataType: 'json'
		});

		request.done(function(data){

			if (data.gender == 1){
				$("#checkbox1_8").prop('checked', true);
			}else{
				$("#checkbox1_9").prop('checked', false);
			}

			if (data.consangunity == 1){
				$("#checkbox34").prop('checked', true);
			}else{
				$("#checkbox34").prop('checked', false);
			}

			$("#dob").val(data.dob);
			calcAge();
			$("#name").val(data.name);


		});
	}



	$("#addmedicine").click(function(){
		action='<td><a class="btn red" onClick = "$(this).parent().parent().remove()"> remove</a></td>'

		var row= '<tr><td>' + $("#medicine :selected").text() + "</td><td>"+$("#dosage").val()+"</td>" + action + "</tr>";

		$("#medicinetable").append(row);

		console.log(drugs);

	});


	$("#addquestion").click(function(){
		action='<td><a class="btn red" onClick = "$(this).parent().parent().remove()"> remove</a></td>'
		var row= '<tr><td>' + $("#question :selected").text() + "</td><td>"+$("#answer").val()+"</td>" + action + "</tr>";
		$("#questiontable").append(row);
		
		var questiondata = {};
		questiondata[$("#question :selected").text().trim()] = $("#answer").val().trim();
		questions.push(questiondata);

	});


	$('#okmessage').on('hidden.bs.modal', function () {
		window.location.href = '{{route("visit", $visit->id)}}'
	});


	$("#savebtn").click(function(){



		if ($("#patients").val() == ""){
			showError('please select a patient');
			return;
		}

		if ($("#name").val().trim() == ""){
			showError('please enter a new name for the patient');
			return;
		}

		if ($("#dob").val() == ""){
			showError('please enter a new birthdate for the patient');
			return;
		}

		if ($("#service").val() == ""){
			showError('please choose a service');
			return;
		}


		if ($("#complaint").val() == ""){
			showError('please set a complaint');
			return;
		}

		if ($("#investigation").val() == ""){
			showError('please set an investigation');
			return;
		}


		$("#medicinetable").each(function(){
			drugs = [];
			var tr = $(this).find("tr");
			tr.each(function(){
				var td = $(this).find('td');
				var drug = '';
				var dosage = '';
				var index = 0;
				td.each(function(){
					index++;
					if(index==1){
						drug = $(this).text().trim();
					}else if(index==2){
						dosage = $(this).text().trim();
					}
				});
			var medicinedata = {};
			medicinedata[drug.trim()] = dosage.trim();

			drugs.push(medicinedata);
			});

		});



		$("#questiontable").each(function(){
			questions = [];
			var tr = $(this).find("tr");
			tr.each(function(){
				var td = $(this).find('td');
				var question = '';
				var answer = '';
				var index = 0;
				td.each(function(){
					index++;
					if(index==1){
						question = $(this).text().trim();
					}else if(index==2){
						answer = $(this).text().trim();
					}
				});
			var questiondata = {};
			questiondata[question.trim()] = answer.trim();

			questions.push(questiondata);
			});

		});

		console.log(drugs);
		var data = {
			service_id : $("#service").val(),
			visit_date : $("#visit").val(),
			revisit_date : $("#revisit").val(),

			complaint : $("#complaint").val(),
			investigation : $("#investigation").val(),
			prevhistory : $("#prevhistory").val(),

			questions : questions,
			drugs : drugs
		};

		data['_token'] = "{{csrf_token()}}";

		console.log(data);

		var request = $.ajax({
			url : '{{route("posteditvisit", $visit->id)}}',
			type : "POST",
			data : data,
			dataType : 'html'
		});
		request.done(function(msg){
			$("#okmessage").modal('show');
		});

	});

});

</script>