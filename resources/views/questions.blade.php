@extends('templates.default-without')

@section('content')

			<div class="container-fluid">
				<div class="page-content">
					<!-- BEGIN BREADCRUMBS -->
					<div class="breadcrumbs">
						<ol class="breadcrumb">
							<li>
								<a href="{{route('root')}}">Home</a>
							</li>
							<li>
								<a href="#">Settings</a>
							</li>
							<li class="active">Questions</li>
						</ol>
					</div>
					<!-- END BREADCRUMBS -->
					<!-- BEGIN PAGE BASE CONTENT -->
					<!--start history table-->
					<div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-question-circle
 								font-white" style="font-size: 18px;"></i>Questions 
 								</div>
 								</div>


<!-------------------->
 
                                <div class="portlet-body">
                                

<!---->
                 <div class="row">
                
                @if(Session::has('info'))
                    <div class="alert alert-info">
                    {{Session::get('info')}}
                    </div>
                @endif


                @if(Session::has('error'))
                    <div class="alert alert-danger">
                    {{Session::get('error')}}
                    </div>
                @endif

                @if(count($errors))
                  <div class="alert alert-danger">
                  @foreach($errors->all() as $error)
                    {{$error}}<br>
                    @endforeach
                  </div>
                @endif
                </div>

<div class="portlet light bordered">
<form action="{{route('putquestions')}}" method="post">  

 
                            <!--start 1st row-->
<div class="row">
	                            <!--start 1st col-->
        <div class="col-md-7">
        	<div class="form-group">
                <label class="col-md-2 control-label">Question</label>
                <div class="col-md-10">
                <div class="input-icon right">
                                    	
            	<input required name="text" type="text" class="form-control" placeholder="Enter Question">
                </div>
                </div>
            </div>
        </div>
<!--end 1st col-->	
<!--start 2nd col-->	                            	                            
            <div class="col-md-4">
                <br>
            	<label class="control-label">
                <!-- <input type="checkbox">
           		Active </label> -->
       		</div>
<!--end 2nd col-->	
     </div>
<!--end 1st row-->

<!--start 2nd row-->
    <div class="row">
                            <!--1st col-->
        <div class="col-md-12">
            <div class="form-group">
                <!-- <label class="control-label col-md-1" >Answer</label>
                <textarea class="form-control col-md-10 " rows="2">
                                	
                </textarea> -->
        	</div>
        	<!--end 1st col-->
    	</div>
	</div>	
	<!--end 1st row-->
<div class="row">
<div class="col-md-12">
	 <div class="actions"  style="padding-bottom: 10px;">
                        <br>
        
            <div class="clearfix">
            <button class="btn green" type="submit">Save</button>
			<input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>
            </form>
            </div>
            
		</div>
	</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<!--start question table-->
<form action="{{route('deletequestions')}}" method="post"> 

	<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
        <thead>
            <tr>
                <th>
                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                <th> Question</th>
            </tr>
        </thead>
         <tbody>
			@foreach($questions as $question)
	    			<tr class="odd gradeX">
	    			    <td>
	    			        <input name="{{$question->id}}" type="checkbox" class="checkboxes" value="1" /> </td>
	    			    <td>{{$question->text}}</td>
	    			</tr>
	        @endforeach
			    </tbody>
			    </table>

			    <button class="btn yellow-saffron" type="submit">Delete</button>
				<input type="hidden" name="_method" value="delete"/>
				<input type="hidden" name="_token" value="{{csrf_token()}}"/>
		</form>


<!--end question table-->
		</div>	
		</div>
<!--end portlet of all content-->  	
      </div>                       
    </div>
</div>

@stop