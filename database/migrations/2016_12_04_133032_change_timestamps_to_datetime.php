<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTimestampsToDatetime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE patients MODIFY dob date');
        DB::statement('alter table visits modify visit_date date NULL');
        DB::statement('alter table visits modify `re-visit_date` date NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table patients modify dob timestamp');
        DB::statement('alter table visits modify visit_date timestamp NULL');
        DB::statement('alter table visits modify `re-visit_date` timestamp NULL');

    }
}
