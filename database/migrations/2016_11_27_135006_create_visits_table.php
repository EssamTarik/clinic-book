<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('complaint');
            $table->text('questions');
            $table->text('medicine');
            $table->integer('patient_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->timestamp('visit_date')->nullable();
            $table->timestamp('re-visit_date')->nullable();

            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('service_id')->references('id')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visits');
    }
}
