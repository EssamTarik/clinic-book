<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VisitsQuestionsMedicineNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE visits MODIFY questions text null');
        DB::statement('ALTER TABLE visits MODIFY medicine text null');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE visits MODIFY questions text not null');
        DB::statement('ALTER TABLE visits MODIFY medicine text not null');
    }
}
